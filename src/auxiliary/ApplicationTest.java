package auxiliary;

import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.TaggedWord;

import java.io.*;
import java.text.BreakIterator;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Used to create and run tests on the suggestion system.
 *
 * @author Acácio Correia
 */
public class ApplicationTest
{
    private Document document;

    // The list of changes that were applied to the selected region
    private final List<Error> changes;

    // The verbosity of the output
    private final boolean verbose;

    // If it si true then the classifier will accept the stem as the correct form
    private final boolean acceptStems;

    // If true then it replaces the old words with synonyms, otherwise it replaces them with garbage without meaning
    private final boolean replaceWithSynonyms;

    // True if you want to retrieve the closest documents, instead of all the documents under a threshold.
    private final boolean fixedNumberDocuments;

    // True if you want all numbers to be replaced by a special tag.
    private final boolean noSpecificNumbers;

    // True if all suggestions must be considered, instead of a limited number which is more realistic and user friendly
    private final boolean allSuggestions;

    // True if type 2 errors should be created
    private final boolean includeType2Errors;

    // True if it should include the special tags at the beginning and end
    private final boolean includeBeginEndTags;

    // The number of files that have been analysed
    private int numberFilesAnalysed;

    // The number of closest documents used in total
    private int numberClosestDocuments;

    // The threshold used to choose the closest documents
    private final float fileThreshold;

    private final List<List<Integer>> pointsSuggestionType;
    private final List<List<Integer>> exclusivePointsSuggestionType;

    // The number of error types that exist
    private final int numErrorTypes;

    // The total points given to the system
    private List<Integer> totalPoints;

    // The max points given to this test
    private List<Integer> maxPoints;

    // The mrr points given to this test
    private List<Float> mrrPoints;

    private final String suggestionTableName;
    private final boolean suggestionFileSeparated;

    // Auxiliary
    // ---------------------------------------------------------------

    // Used for breaking strings into sentences
    private final BreakIterator sentenceIterator;

    // Used for the calculation of topics
    private final LDA topicsModel;

    // Used to calculate suggestions
    private final CalculateSuggestions cs;

    // Used to parse sentences
    private final StanfordParser sp;

    public PrintWriter logFile;

    public static void main(String[] args)
    {
        long initTime = System.currentTimeMillis();

        boolean verbose = false;

        boolean acceptStems = false;
        boolean replaceWithSynonyms = true;
        boolean fixedNumberDocuments = false;
        boolean noSpecificNumbers = true;
        boolean allSuggestions = false;
        boolean includeType2Errors = false;

        String suggestionTableName = "csTotal";
        String fileTableName = "file_arxiv";
        boolean suggestionFileSeparated = false;
        boolean includeBeginEndTags = true;

        boolean probabilityKN = false;

        //String topicsStateFile = "D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\corpusState.txt";
        //String filePath = "D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\aclArc";

        String topicsStateFile = "D:\\Desktop\\arxiv\\csCorpusState850.txt";
        String filePath = "D:\\Desktop\\arxiv\\text\\math - 5115 - text";

        //String topicsStateFile = "D:\\Desktop\\arxiv\\mathCorpusState1950.txt";
        //String filePath = "D:\\Desktop\\arxiv\\text\\math - 5115 - text";

        float fileThreashold = 6f;

        ApplicationTest at = new ApplicationTest(suggestionTableName, fileTableName, suggestionFileSeparated, topicsStateFile,
                verbose, acceptStems, replaceWithSynonyms, fixedNumberDocuments, noSpecificNumbers, allSuggestions, includeType2Errors, includeBeginEndTags, fileThreashold, probabilityKN);

        at.logFile.println("\nPROCESS START at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(System.currentTimeMillis()) + "\n----------------");

        File file = new File(filePath);

        int numberFiles = 10000000;

        at.testDirectory(file, numberFiles);

        at.printFinalResult(file.getName());

        at.logFile.println("Total time: " + (System.currentTimeMillis() - initTime) + "ms");

        at.logFile.close();
    }

    public ApplicationTest(String suggestionTableName, String fileTableName, boolean suggestionFileSeparated, String topicsStateFile)
    {
        this(suggestionTableName, fileTableName, suggestionFileSeparated,
                topicsStateFile, false, false, false, false, true, false, true, true, 3.0f, false);
    }

    public ApplicationTest(String suggestionTableName, String fileTableName, boolean suggestionFileSeparated, String topicsStateFile, boolean verbose, boolean acceptStems, boolean replaceWithSynonyms,
                           boolean fixedNumberDocuments, boolean noSpecificNumbers, boolean allSuggestions, boolean includeType2Errors, boolean includeBeginEndTags, float fileThreshold, boolean probabilityKN)
    {
        this.suggestionTableName = suggestionTableName;
        this.suggestionFileSeparated = suggestionFileSeparated;

        this.verbose = verbose;
        this.acceptStems = acceptStems;
        this.replaceWithSynonyms = replaceWithSynonyms;
        this.fixedNumberDocuments = fixedNumberDocuments;
        this.fileThreshold = fileThreshold;

        this.noSpecificNumbers = noSpecificNumbers;
        this.allSuggestions = allSuggestions;
        this.includeType2Errors = includeType2Errors;

        this.includeBeginEndTags = includeBeginEndTags;

        numberFilesAnalysed = 0;
        numberClosestDocuments = 0;

        sentenceIterator = BreakIterator.getSentenceInstance(new Locale("en", "GB"));

        changes = new ArrayList<>();

        topicsModel = new LDA(false);

        if (suggestionFileSeparated)
            topicsModel.importSavedState(topicsStateFile);

        // Used to interact with the DB
        NGramDB db = new NGramDB(verbose, probabilityKN);

        if (suggestionFileSeparated)
            db.getInitialFile(fileTableName, suggestionTableName);

        cs = new CalculateSuggestions(suggestionTableName, suggestionFileSeparated, verbose, db);
        sp = cs.getSp();

        numErrorTypes = 4;

        totalPoints = new ArrayList<>();
        maxPoints = new ArrayList<>();
        mrrPoints = new ArrayList<>();

        // The two types of error that exist currently
        for (int i = 0; i < numErrorTypes; i++)
        {
            totalPoints.add(0);
            maxPoints.add(0);
            mrrPoints.add(0.0f);
        }

        // The point system used to calculate the impact of each type of suggestion
        pointsSuggestionType = new ArrayList<>();
        exclusivePointsSuggestionType = new ArrayList<>();

        List<Integer> x, y;

        for (int i = 0; i < SuggestedReplacement.NUMBER_TYPES; i++)
        {
            x = new ArrayList<>();
            y = new ArrayList<>();

            for (int j = 0; j < numErrorTypes; j++)
            {
                x.add(0);
                y.add(0);
            }

            pointsSuggestionType.add(x);
            exclusivePointsSuggestionType.add(y);
        }

        try
        {
            logFile = new PrintWriter(new BufferedWriter(new FileWriter("applicationTest.log", true)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Randomly select a sentence from the {@code completeText} between the 5th and the 15th and apply changes to that sentence, returning the result.
     *
     * @param completeText the original text.
     *
     * @return the modified sentence.
     */
    public String createTests(String completeText)
    {
        // If the text is empty
        if (completeText.equals(""))
        {
            System.err.println("ERROR: The file you provided was empty.");
            return null;
        }

        changes.clear();

        String region = null;

        sentenceIterator.setText(completeText);
        int start = sentenceIterator.first();

        int currSent = 0;

        // Randomly select the number of the sentence of interest
        // Exclude the first 10 sentences as these are normally the worst in terms of parsing
        int selectedSentenceNumber = new Random().nextInt(50) + 10;

        // Iterate over the sentences of the text until we get to the selectedSentenceNumber
        for (int end = sentenceIterator.next();
             end != BreakIterator.DONE;
             start = end, end = sentenceIterator.next())
        {
            // If it has reached the selected sentence
            if (currSent == selectedSentenceNumber)
            {
                region = completeText.substring(start, end);
                break;
            }

            currSent++;
        }

        // If the text didn't have that many sentences get the last sentence
        if (region == null)
            return null;

        if (region.length() <= 15)
        {
            System.err.println("ERROR: The region was too small.");
            return createTests(completeText);
        }

        if (verbose)
            System.out.println("\nOriginal region:\n------------------------\n" + region);

        List<String> relevantTerms = getMostRelevantTerms(completeText, region, 5);

        if (verbose)
        {
            System.out.println("\nMost relevant terms in the region:\n------------------------");

            relevantTerms.forEach(System.out::println);
        }

        String newRegion = getModifiedRegion(region, relevantTerms, replaceWithSynonyms);

        if (changes.isEmpty())
        {
            System.err.println("ERROR: There were no modifications applied to the selected region.");

            // Calls the function again to change a different region
            // This can potentially create an infinite cycle
            return createTests(completeText);
        }

        if (verbose)
            System.out.println("\nRegion after modifications have been applied:\n------------------------\n" + newRegion);

        return newRegion;
    }

    /**
     * Calculate the suggestions for a sentence {@code newRegion}, given the complete text {@code completeText} in which it was inserted in.
     *
     * @param completeText         the complete text in which the sentence is inserted.
     * @param newRegion            the text to be analysed.
     * @param fixedNumberDocuments true if you want to retrieve the closest documents, instead of all the documents under a threshold.
     * @param noSpecificNumbers    true if you want all numbers to be replaced by a special tag.
     *
     * @return a list of sentences with the suggestions proposed by the system.
     */
    public List<Sentence> analyseSentence(String completeText, String newRegion, boolean fixedNumberDocuments, boolean noSpecificNumbers)
    {
        if (suggestionFileSeparated)
        {
            // The topic distribution for the new document
            double[] topicDistribution = topicsModel.distributionNewDocument(completeText);

            List<Integer> closestDocuments;

            if (fixedNumberDocuments)
                closestDocuments = topicsModel.getClosestDocuments(topicDistribution, 100);
            else
                closestDocuments = topicsModel.getDocumentsUnderThreshold(topicDistribution, fileThreshold);

            document.setContext(new FileContext(closestDocuments));

            logFile.println("Number of selected documents: " + closestDocuments.size());

            numberClosestDocuments += closestDocuments.size();
        }

        List<Sentence> sentences;

        if (allSuggestions)
            sentences = cs.allSuggestions(newRegion, noSpecificNumbers, includeBeginEndTags);
        else
        {
            sentences = new ArrayList<>();
            cs.ngramWordnetSuggestions(newRegion, sentences, noSpecificNumbers, includeBeginEndTags);
        }

        Suggestion s;
        int i;

        if (verbose)
        {
            System.out.println("\nSuggestions to the modified region:\n------------------------");

            String cSent;

            // Iterate over all the existing suggestions
            for (i = 0; i < sentences.size(); i++)
            {
                cSent = sentences.get(i).getSentence();

                for (int j = 0; j < sentences.get(i).getSuggestions().size(); j++)
                {
                    s = sentences.get(i).getSuggestions().get(j);

                    System.out.println("Replace \"" + cSent.substring(s.getFromPos(), s.getToPos()) + "\" with \"" + s.getReplacements() + "\" from " + s.getFromPos() + " to " + s.getToPos());
                }
            }
        }

        return sentences;
    }

    /**
     * Given the {@code changes} applied and {@code suggestions} given by the system, calculates a number of points, according to whether that change was referred by a suggestion and, if so, in which position.
     *
     * @param changes      the list of changes that were applied to the selected region.
     * @param sentenceList the list of sentences.
     */
    public void classifySuggestions(List<Error> changes, List<Sentence> sentenceList)
    {
        if (changes.isEmpty())
        {
            System.err.println("ERROR: The changes list is empty.");
            return;
        }

        int pos;

        boolean found;
        int posFound;

        TextChange t;

        List<Integer> max = new ArrayList<>();
        List<Integer> points = new ArrayList<>();

        List<Float> mrr = new ArrayList<>();

        for (int i = 0; i < numErrorTypes; i++)
        {
            max.add(0);
            points.add(0);
            mrr.add(0.0f);
        }

        // Calculates the points given the suggestions and changes
        for (Error change : changes)
        {
            t = change.getChange();

            found = false;
            posFound = -1;

            // Increase the max for this file
            max.set(change.getType(), max.get(change.getType()) + 5);

            // TODO: THIS COULD BE IMPROVED SINCE NOT ALL SENTENCES ARE AFFECTED BY THIS ERROR
            // TODO: ASSUMES THE REGION CONTAINS ONLY ONE SENTENCE
            for (Sentence sent : sentenceList)
            {
                for(Suggestion s : sent.getSuggestions())
                {
                    List<SuggestedReplacement> replacements = s.getReplacements();

                    // Suggestions refers to the errors under analysis
                    if (t.getStartPosition() == s.getFromPos())
                    {
                        pos = replacements.indexOf(new SuggestedReplacement(SuggestedReplacement.NONE, t.getOldValue()));

                        if (pos != -1)
                        {
                            SuggestedReplacement sr = replacements.get(pos);

                            if (sr.getTypes().isEmpty())
                                continue;

                            if (sr.getTypes().size() == 1)
                                if (sr.getTypes().get(0) != -1)
                                    exclusivePointsSuggestionType.get(sr.getTypes().get(0)).set(change.getType(), exclusivePointsSuggestionType.get(sr.getTypes().get(0)).get(change.getType()) + 5);

                            for (int type : sr.getTypes())
                                if (type != -1)
                                    pointsSuggestionType.get(type).set(change.getType(), pointsSuggestionType.get(type).get(change.getType()) + 5);

                            found = true;
                            posFound = pos;
                        }
                    }
                }

                break;
            }

            if (found)
            {
                points.set(change.getType(), points.get(change.getType()) + 5);
                mrr.set(change.getType(), mrr.get(change.getType()) + (1/(float)(posFound + 1)));
            }
            else
                logFile.println("Failed to correct: " + change);
        }

        for (int i = 0; i < numErrorTypes; i++)
        {
            String s = String.format("ERROR %d -> %3d/%3d points with MRR = %1.3f.", i, points.get(i), max.get(i), (mrr.get(i)/(max.get(i)/5)));
            logFile.println(s);

            if (points.get(i) > max.get(i))
                System.err.println("ERROR in classifySuggestions: Got more points than the max number of points.");
        }

        // Update the points
        for (int i = 0; i < numErrorTypes; i++)
        {
            totalPoints.set(i, totalPoints.get(i) + points.get(i));
            maxPoints.set(i, maxPoints.get(i) + max.get(i));
            mrrPoints.set(i, mrrPoints.get(i) + mrr.get(i));
        }
    }

    /**
     * Creates tests for each file in the directory.
     * It checks for other directories inside this directory.
     *
     * @param file the directory file.
     */
    public void testDirectory(File file, int numberFiles)
    {
        if(numberFilesAnalysed >= numberFiles)
            return;

        if (file.isDirectory())
        {
            if (file.getName().equals("Train"))
                return;

            if (file.getName().equals("Validation"))
                return;

            File[] listOfFiles = file.listFiles();

            if (listOfFiles != null)
            {
                // Create tests for each of the files in the directory until you reach the limit or there are no more files
                for (File listOfFile : listOfFiles)
                {
                    testDirectory(listOfFile, numberFiles);

                    logFile.flush();
                }
            }
        }
        else
        {
            testFile(file);
        }
    }

    /**
     * Create changes in the original text from the file {@code file} and check what are the suggestions given by the system.
     *
     * @param file the file of interest.
     */
    public void testFile(File file)
    {
        document = new Document();
        cs.setDocument(document);

        String completeText = FileHandler.readTXTText(file);

        String newRegion = createTests(completeText);

        // If the text is empty
        if (newRegion == null)
            return;

        System.out.print("\rFile number: " + numberFilesAnalysed++);

        List<Sentence> sentenceList = analyseSentence(completeText, newRegion, fixedNumberDocuments, noSpecificNumbers);

        logFile.println("\nResult for file \"" + file.getName() + "\":\n");

        classifySuggestions(changes, sentenceList);
    }

    /**
     * Returns the result from the log of a number with any base.
     *
     * @param x    the number of interest.
     * @param base the base for the log.
     *
     * @return the result from the log of a number with any base.
     */
    private double logAnyBase(double x, double base)
    {
        if (base == 1)
        {
            System.err.println("log1 is undefined.");

            return 0;
        }

        return Math.log(x) / Math.log(base);
    }

    /**
     * Returns the most relevant terms of a certain region from a text.
     *
     * @param completeText the complete original text.
     * @param region       the text of the region.
     * @param numberTerms  the number of terms we're interested in.
     *
     * @return the most relevant terms of a certain region from a text.
     */
    public List<String> getMostRelevantTerms(String completeText, String region, int numberTerms)
    {
        NGram textNgrams = new NGram(1);
        textNgrams.calculateNgramCountsText(completeText, true, false);

        NGram regionNgrams = new NGram(1);
        regionNgrams.calculateNgramCountsText(region, true, false);

        Iterator<Map.Entry<List<String>, Integer>> it = regionNgrams.getNgramCounts().entrySet().iterator();

        // These are used to calculate the 'numberWordsInterested' most probable expressions 
        List<Double> termRelevance = new ArrayList<>();
        List<String> terms = new ArrayList<>();

        int j;

        double relevance, probability;

        // Get the probability of each expression and discover the most probable expressions
        while (it.hasNext())
        {
            Map.Entry<List<String>, Integer> entry = it.next();
            List<String> ngram = entry.getKey();

            probability = textNgrams.getProbabilityNgram(ngram);

            if (probability == 0)
            {
                relevance = 0;
            }
            else
            {
                relevance = (ngram).get(0).length() * logAnyBase(regionNgrams.getProbabilityNgram(ngram) / probability, 2);
            }

            // If the list is empty - add the new probability
            if (termRelevance.isEmpty())
            {
                termRelevance.add(relevance);
                terms.add(ngram.get(0));
            }
            // If this has bigger probability than the one with the least probability - place it at the correct position
            else if (relevance > termRelevance.get(termRelevance.size() - 1))
            {
                for (j = termRelevance.size() - 1; j >= 0; j--)
                    if (relevance < termRelevance.get(j))
                        break;

                termRelevance.add(j + 1, relevance);
                terms.add(j + 1, ngram.get(0));

                // If the size of the list is bigger than what we want, remove the one with the least probability
                if (termRelevance.size() > numberTerms)
                {
                    termRelevance.remove(termRelevance.size() - 1);
                    terms.remove(terms.size() - 1);
                }
                // If there's space left - place it at the end
            }
            else if (termRelevance.size() < numberTerms)
            {
                termRelevance.add(relevance);
                terms.add(ngram.get(0));
            }
        }

        return terms;
    }

    /**
     * Get the region after applying the modification.
     *
     * @param region              the region of interest.
     * @param relevantTerms       the terms with the most relevance in the region.
     * @param replaceWithSynonyms if true then it replaces the old words with synonyms, otherwise it replaces them with garbage without meaning.
     *
     * @return the region after applying the modification.
     */
    public String getModifiedRegion(String region, List<String> relevantTerms, boolean replaceWithSynonyms)
    {
        String dummyWord = "akocu";

        JWI wordnet = new JWI(verbose);

        List<TaggedWord> taggedWordList;
        TaggedWord t;

        POS wordTag;

        List<String> synonyms;

        Random rng = new Random();

        int errorType, begin;

        boolean isPunctuation;

        // The difference in terms of position, from the original region
        int newRegionDiff;

        SimpleNLG nlg = new SimpleNLG(verbose);

        if (verbose)
            System.out.println("\nChanges applied to the selected region:\n------------------------");

        sentenceIterator.setText(region);
        int start = sentenceIterator.first();

        // Iterate over the sentences of the text
        for (int end = sentenceIterator.next();
             end != BreakIterator.DONE;
             start = end, end = sentenceIterator.next())
        {
            // Parse the sentence with the Stanford Parser
            taggedWordList = sp.parseSentence(region.substring(start, end));

            newRegionDiff = 0;

            // Most likely the sentence was too long and an exception was thrown
            // This will return the original region, making it restart the choice of the sentence
            if (taggedWordList == null)
            {
                return region;
            }

            // Iterate over all the words in the text string
            for (int i = 0; i < taggedWordList.size(); i++)
            {
                isPunctuation = false;

                t = taggedWordList.get(i);

                String word = t.word();

                // The word is not on the list of the most relevant words then ignore it
                if (!relevantTerms.contains(word) && !t.tag().equals("IN"))
                    continue;

                String newWord;

                // 50% chance of generating a test for removing a word
                if (rng.nextInt(10) > 7 && includeType2Errors)
                {
                    // No more words so don't do anything
                    if (i + 1 >= taggedWordList.size())
                        break;

                    newWord = "";

                    errorType = 2;
                    begin = taggedWordList.get(i + 1).beginPosition() + (newWord.length() - word.length());

                    wordTag = null;
                }
                else
                {
                    // Replace a proposition with a different proposition
                    if (t.tag().equals("IN"))
                    {
                        errorType = 1;
                        begin = t.beginPosition();

                        newWord = word;

                        wordTag = null;

                        while (newWord.equals(word))
                        {
                            newWord = CalculateSuggestions.prepositions[rng.nextInt(CalculateSuggestions.prepositions.length - 1)];
                        }
                    }
                    else
                    {
                        errorType = 3;
                        begin = t.beginPosition();

                        // Get the POS of the word in the sentence
                        wordTag = sp.getPOSWordnet(t.tag());

                        if (replaceWithSynonyms)
                        {
                            // If there is no POS of interest
                            if (wordTag == null)
                            {
                                if (word.matches("[.,!;:'`\"]"))
                                    isPunctuation = true;

                                newWord = dummyWord;
                            }
                            else
                            {
                                synonyms = wordnet.getSynonyms(word, wordTag);

                                if (synonyms == null)
                                    newWord = dummyWord;
                                else
                                {
                                    // If it doesn't include at least 1 synonym beyond the original word
                                    if (synonyms.size() < 2)
                                        newWord = dummyWord;
                                    else
                                    {
                                        errorType = 0;

                                        // A random position from the synonyms, excluding the first one (which should be the original word)
                                        int rPos = rng.nextInt(synonyms.size() - 1) + 1;

                                        nlg.setWord(synonyms.get(rPos), t.tag());

                                        // TODO: THIS CONSIDERS THAT THE RESULTS FROM SIMPLENLG ALWAYS RESULTS IN ONLY ONE WORD - WHICH SEEMS TO BE TRUE
                                        newWord = nlg.getVariants().get(0);
                                    }
                                }
                            }
                        }
                        // Replaces the words with a garbage string meaning nothing
                        else
                            newWord = dummyWord;
                    }
                }

                // If the new word is exactly like the first one then exclude this as a change
                if (word.equals(newWord))
                    continue;

                // Apply the text change and create an error
                // ------------------------------------------------------------------------------

                if(isPunctuation)
                    newRegionDiff ++;

                if (acceptStems)
                    changes.add(new Error(errorType, new TextChange(wordnet.wordnetStemming(word, wordTag), newWord, begin + newRegionDiff)));
                else
                    changes.add(new Error(errorType, new TextChange(word.toLowerCase(), newWord, begin + newRegionDiff)));

                if (verbose)
                    System.out.println(changes.get(changes.size() - 1));

                // This prevents changing words that influence other changed others, causing problems
                // TODO: THIS IS A RESTRICTION - INCREASES SUCCESS
                i += 3;

                // Add space for the word to be spaced from the previous word
                if(isPunctuation)
                {
                    newRegionDiff--;
                    newWord = " " + newWord;
                }

                // Change the region
                region = region.substring(0, Math.max((begin + newRegionDiff), 0)) + newWord + region.substring(Math.max(begin + newRegionDiff + word.length(), 0), region.length());

                newRegionDiff += (newWord.length() - word.length());
            }
        }

        return region;
    }

    public void printTypeDistribution(String typeName, int pos, int errorType)
    {
        String s = String.format("%13s: %4d points = %6.2f%% and %4d exclusive points = %6.2f%%.", typeName, pointsSuggestionType.get(pos).get(errorType), ((float) pointsSuggestionType.get(pos).get(errorType) / totalPoints.get(errorType) * 100),
                exclusivePointsSuggestionType.get(pos).get(errorType), ((float) exclusivePointsSuggestionType.get(pos).get(errorType) / totalPoints.get(errorType) * 100));

        logFile.println(s);
    }

    public void printTotalTable()
    {
        int tp = 0, mp = 0;
        float mrrp = 0;

        for (int i = 0; i < numErrorTypes; i++)
        {
            tp += totalPoints.get(i);
            mp += maxPoints.get(i);
            mrrp += mrrPoints.get(i);
        }

        int total, exc;

        String s;
        logFile.print("All & " + mp + " & ");

        // Types of suggestions
        for (int i = 0; i < 6; i++)
        {
            total = 0;
            exc = 0;

            for (int j = 0; j < numErrorTypes; j++)
            {
                total += pointsSuggestionType.get(i).get(j);
                exc += exclusivePointsSuggestionType.get(i).get(j);
            }

            s = String.format("%6.2f / %6.2f", ((float) total / tp * 100), ((float) exc / tp * 100));

            logFile.print(s);
        }

        s = String.format("%6.2f & %1.2f \\\\ \\hline", ((float) tp / mp * 100), (mrrp/(mp/5)));
        logFile.print(s);
    }

    public void printTypeTable(int pos, int errorType)
    {
        String s = String.format("%6.2f / %6.2f", ((float) pointsSuggestionType.get(pos).get(errorType) / totalPoints.get(errorType) * 100), ((float) exclusivePointsSuggestionType.get(pos).get(errorType) / totalPoints.get(errorType) * 100));

        logFile.print(s);
    }

    public void printTable()
    {
        for (int i = 0; i < numErrorTypes; i++)
        {
            logFile.print(i + " & " + maxPoints.get(i) + " & ");

            for(int j = 0; j < 6; j++)
            {
                printTypeTable(j, i);

                logFile.print(" & ");
            }

            String s = String.format("%6.2f & %1.2f \\\\ \\hline", i, ((float) totalPoints.get(i) / maxPoints.get(i) * 100), (mrrPoints.get(i) / (maxPoints.get(i) / 5)));
            logFile.println(s);
        }

        printTotalTable();
    }

    /**
     * Print the final result for the file or directory analysed.
     *
     * @param fileName the name of the file or directory analysed.
     */
    public void printFinalResult(String fileName)
    {
        int tp = 0, mp = 0;
        float mrrp = 0;

        logFile.println("\n\nFile \"" + fileName + ":");

        logFile.println("PARAMETERS:");
        logFile.println("suggestionTableName:     " + suggestionTableName);
        logFile.println("suggestionFileSeparated: " + suggestionFileSeparated);
        logFile.println("includeBeginEndTags:     " + includeBeginEndTags);

        if (suggestionFileSeparated)
            logFile.println("fileTreshold:            " + fileThreshold);

        logFile.println("acceptStems:             " + acceptStems);
        logFile.println("replaceWithSynonyms:     " + replaceWithSynonyms);
        logFile.println("fixedNumberDocuments:    " + fixedNumberDocuments);
        logFile.println("noSpecificNumbers:       " + noSpecificNumbers);
        logFile.println("allSuggestions:          " + allSuggestions);

        logFile.println("\nNUMBER OF FILES ANALYSED: " + numberFilesAnalysed);

        if (suggestionFileSeparated)
            logFile.println("\nAVERAGE NUMBER OF CLOSEST DOCUMENTS USED: " + (numberClosestDocuments / (float) numberFilesAnalysed));

        logFile.println("\nRESULTS");

        for (int i = 0; i < numErrorTypes; i++)
        {
            String s = String.format("ERROR %d: %4d/%4d points = %6.2f%% => MRR = %1.2f.", i, totalPoints.get(i), maxPoints.get(i), ((float) totalPoints.get(i) / maxPoints.get(i) * 100), (mrrPoints.get(i)/(maxPoints.get(i)/5)));
            logFile.println(s);

            tp += totalPoints.get(i);
            mp += maxPoints.get(i);
            mrrp += mrrPoints.get(i);
        }

        String s = String.format("TOTAL:   %4d/%4d points = %6.2f%% => MRR = %1.2f.", tp, mp, ((float) tp / mp * 100), (mrrp/(mp/5)));
        logFile.println(s);

        logFile.println("\nSUGGESTION TYPE POINT DISTRIBUTION:");

        for (int i = 0; i < numErrorTypes; i++)
        {
            logFile.println("- ERROR " + i + ":");

            printTypeDistribution("Language Tool", 0, i);
            printTypeDistribution("Synonyms", 1, i);
            printTypeDistribution("After", 2, i);
            printTypeDistribution("Between", 3, i);
            printTypeDistribution("Before", 4, i);
            printTypeDistribution("Prepositions", 5, i);
        }

        printTable();
    }
}