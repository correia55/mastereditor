package auxiliary;

import org.w3c.dom.Document;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acácio Correia on 14/06/2016.
 */
public class Arxiv
{
    static class PDFFile
    {
        public String url;
        public String category;
    }

    public static List<PDFFile> parseXML(String category, int start, int maxResults)
    {
        return parseXML("http://export.arxiv.org/api/query?search_query=cat:" + category + "&start=" + start + "&max_results=" + maxResults);
    }

    public static List<PDFFile> parseXML(String url)
    {
        List<PDFFile> pdfs = new ArrayList<>();

        PDFFile pdf;

        try
        {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(url);

            doc.getDocumentElement().normalize();

            NodeList entries = doc.getElementsByTagName("entry");

            Node entry;

            for (int i = 0; i < entries.getLength(); i++)
            {
                entry = entries.item(i);

                pdf = new PDFFile();

                if (entry.getNodeType() == Node.ELEMENT_NODE)
                {
                    Element eElement = (Element) entry;

                    NodeList links = eElement.getElementsByTagName("link");

                    for (int j = 0; j < links.getLength(); j++)
                    {
                        NamedNodeMap nnm = links.item(j).getAttributes();

                        Node n = nnm.getNamedItem("title");

                        if (n != null)
                        {
                            // Get the link for the pdf
                            if (n.getNodeValue().equals("pdf"))
                            {
                                pdf.url = nnm.getNamedItem("href").getNodeValue();

                                break;
                            }
                        }
                    }

                    NodeList categories = eElement.getElementsByTagName("category");

                    for (int j = 0; j < categories.getLength(); j++)
                    {
                        NamedNodeMap nnm = categories.item(j).getAttributes();

                        Node n = nnm.getNamedItem("term");

                        if (n != null)
                        {
                            pdf.category = n.getNodeValue();

                            break;
                        }
                    }

                    pdfs.add(pdf);
                }
            }
        }
        catch (ParserConfigurationException | SAXException | IOException x)
        {
            x.printStackTrace();
        }

        return pdfs;
    }

    public static void getPDF(List<PDFFile> pdfs)
    {
        for (PDFFile p : pdfs)
        {
            System.out.println(p.url + " -> " + p.category);

            try
            {
                URL website = new URL(p.url);
                ReadableByteChannel rbc = Channels.newChannel(website.openStream());

                String fileName = "D:\\Desktop\\arxiv\\" + p.category + "\\" + p.url.replaceFirst("http://arxiv.org/pdf/", "").replace("/", "\\") + ".pdf";

                File f = new File(fileName);

                if (f.exists())
                    continue;

                f.getParentFile().mkdirs();

                FileOutputStream fos = new FileOutputStream(fileName);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

                fos.close();
            }
            catch (IOException x)
            {
                x.printStackTrace();
            }
        }

        System.out.println("All PDFs downloaded!");
    }

    public static void main(String[] args) throws Exception
    {
        getPDF(parseXML("q-bio.SC", 800, 200));
        Thread.sleep(600000);

        /*getPDF(parseXML("q-bio.OT", 0, 300));
        Thread.sleep(600000);

        getPDF(parseXML("q-bio.QM", 0, 200));
        Thread.sleep(600000);

        getPDF(parseXML("q-bio.SC", 0, 400));
        Thread.sleep(600000);

        getPDF(parseXML("q-bio.TO", 0, 400));
        Thread.sleep(600000);*/
    }
}