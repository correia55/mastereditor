package auxiliary;

import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.TaggedWord;
import org.languagetool.rules.RuleMatch;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Class used to calculate the suggestions for a document, according to its context.
 *
 * @author Acácio Correia
 */
public class CalculateSuggestions
{
    // The class that treats the database requests
    private NGramDB db;

    private String suggestionTableName;
    private boolean suggestionFileSeparated;

    private Document document;

    // Accept suggestions when the first suggestion is the current word being used
    private final boolean acceptSecondarySuggestions;

    // The verbosity of the output
    private final boolean verbose;

    // Auxiliary
    // ---------------------------------------------------------------

    // Used for breaking strings into sentences
    private BreakIterator sentenceIterator;

    // Used to get suggestions from the Language Tool
    private LanguageTool lt;

    // Used to get synonyms from the Wordnet
    private JWI wordnet;

    // Used to parse sentences
    private StanfordParser sp;

    public static String[] prepositions = {"aboard", "about", "above", "across", "after", "against", "along", "amid", "among", "anti", "around", "as", "at", "before", "behind", "below", "beneath", "beside", "besides",
            "between", "beyond", "but", "by", "concerning", "considering", "despite", "down", "during", "except", "excepting", "excluding", "following", "for", "from", "in", "inside", "into", "like", "minus",
            "near", "of", "off", "on", "onto", "opposite", "outside", "over", "past", "per", "plus", "regarding", "round", "save", "since", "than", "through", "to", "toward", "towards", "under", "underneath",
            "unlike", "until", "up", "upon", "versus", "via", "with", "within", "without"};

    public CalculateSuggestions(String suggestionTableName, boolean suggestionFileSeparated, NGramDB db)
    {
        this(suggestionTableName, suggestionFileSeparated, db, BreakIterator.getSentenceInstance(new Locale("en", "GB")));
    }

    public CalculateSuggestions(String suggestionTableName, boolean suggestionFileSeparated, boolean verbose, NGramDB db)
    {
        this(suggestionTableName, suggestionFileSeparated, verbose, db, BreakIterator.getSentenceInstance(new Locale("en", "GB")));
    }

    public CalculateSuggestions(String suggestionTableName, boolean suggestionFileSeparated, NGramDB db, BreakIterator sentenceIterator)
    {
        this(suggestionTableName, suggestionFileSeparated, false, db, sentenceIterator);
    }

    public CalculateSuggestions(String suggestionTableName, boolean suggestionFileSeparated, boolean verbose, NGramDB db, BreakIterator sentenceIterator)
    {
        this.verbose = verbose;
        this.suggestionTableName = suggestionTableName;
        this.suggestionFileSeparated = suggestionFileSeparated;

        acceptSecondarySuggestions = true;

        this.db = db;
        this.sentenceIterator = sentenceIterator;

        lt = new LanguageTool();
        wordnet = new JWI(verbose);
        sp = new StanfordParser(verbose);
    }

    public void setDb(NGramDB db)
    {
        this.db = db;
    }

    public void setDocument(Document document)
    {
        this.document = document;
    }

    public StanfordParser getSp()
    {
        return sp;
    }

    /**
     * Add the suggestions to a list from the suggestion rules obtained from the Language Tool.
     *
     * @param sent the rules for the suggestions provided from the Language Tool.
     */
    private void addRuleMatchSuggestionList(Sentence sent)
    {
        List<RuleMatch> rules = lt.checkSentence(sent.getSentence());

        Suggestion s;

        List<Suggestion> suggestionsList = sent.getSuggestions();

        if (verbose)
            System.out.println("\nLanguage Tool Suggestions:\n------------------------");

        for (RuleMatch r : rules)
        {
            s = new Suggestion(r.getFromPos(), r.getToPos());
            s.addReplacementsFromType(r.getSuggestedReplacements(), SuggestedReplacement.LANGUAGE_TOOL);
            suggestionsList.add(s);

            if (verbose)
                System.out.println(s);
        }
    }

    /**
     * Calculate suggestions for the text.
     * These are based on the synonyms of each word obtained from the wordnet, the n-grams and the (n-1) words that come after.
     *
     * @param text                the text to be analysed.
     * @param sentences           the list where the sentences we'll be added to.
     * @param noSpecificNumbers   true if you want all numbers to be replaced by a special tag.
     * @param includeBeginEndTags true if it should include the special tags at the beginning and end.
     */
    public void ngramWordnetSuggestions(String text, List<Sentence> sentences, boolean noSpecificNumbers, boolean includeBeginEndTags)
    {
        if (text == null)
            return;

        sentenceIterator.setText(text);
        int start = sentenceIterator.first();

        Sentence sent;

        // Iterate over the sentences of the text
        for (int end = sentenceIterator.next();
             end != BreakIterator.DONE;
             start = end, end = sentenceIterator.next())
        {
            sent = new Sentence(text.substring(start, end), start, end, true);

            sentences.add(sent);
        }

        ngramWordnetSuggestions(sentences, noSpecificNumbers, includeBeginEndTags);
    }

    /**
     * Calculate suggestions for the text.
     * These are based on the synonyms of each word obtained from the wordnet, the n-grams and the (n-1) words that come after.
     *
     * @param sentences           the sentences to be analyzed.
     * @param noSpecificNumbers   true if you want all numbers to be replaced by a special tag.
     * @param includeBeginEndTags true if it should include the special tags at the beginning and end.
     */
    public void ngramWordnetSuggestions(List<Sentence> sentences, boolean noSpecificNumbers, boolean includeBeginEndTags)
    {
        if (sentences == null)
            return;

        // TODO: This value is the maximum currently at the DB
        short n = 3;
        int i, j;

        List<String> ngram = new ArrayList<>();
        List<String> synonyms, conjSynonyms;
        List<String> nextWords = new ArrayList<>();

        List<TaggedWord> taggedWordList;
        TaggedWord t;

        POS wordTag;

        List<SuggestedReplacement> replacementSuggestions;

        SimpleNLG nlg = new SimpleNLG(verbose);

        Suggestion sug;

        // Iterate over the sentences of the text
        for (Sentence sent : sentences)
        {
            // If this sentence remains the same
            if (!sent.wasEdited())
                continue;

            sent.setWasEdited(false);

            // Add the suggestions from the language tool to the list of suggestions
            addRuleMatchSuggestionList(sent);

            // Parse the sentence with the Stanford Parser
            taggedWordList = sp.parseSentence(sent.getSentence());

            if (taggedWordList == null)
                continue;

            ngram.clear();

            // Adds strings that represent the beginning of the sentence
            if (includeBeginEndTags)
                for (i = 1; i < n; i++)
                    ngram.add("<string" + i + ">");

            // Iterate over all the words in the text string
            for (i = 0; i < taggedWordList.size(); i++)
            {
                t = taggedWordList.get(i);

                String word = t.word();

                // Replace the numbers with <number>
                if (noSpecificNumbers)
                    word = word.replaceAll("-?[0-9]+([.,][0-9]+)?", "<number>");

                // The query for a "." is very slow and since it has no importance I'm removing it
                if (word.equals("."))
                {
                    ngram.clear();
                    continue;
                }

                // TODO: NUMBERS DON'T GET SUGGESTIONS
                if (word.equals("<number>"))
                {
                    if (ngram.size() == (n - 1))
                        // Remove the first element from the list
                        // To continue building with the rest of the words
                        ngram.remove(0);

                    // Add this word to the n-gram
                    ngram.add(word);

                    continue;
                }

                // Skip non word or punctuation
                if (!word.matches("([a-zA-Z'`\"])+|[.,!;:]"))
                {
                    ngram.clear();
                    continue;
                }

                // If the n-gram has the correct size
                if (ngram.size() == (n - 1))
                {
                    // Get the POS of the word in the sentence
                    wordTag = sp.getPOSWordnet(t.tag());

                    if (verbose)
                        System.out.println("\nDB Queries:\n------------------------\nWord of interest: " + word + " (" + t.tag() + ")");

                    sug = new Suggestion(t.beginPosition(), t.endPosition());
                    replacementSuggestions = new ArrayList<>();


                    // Context suggestions
                    // -------------------------------------------------------------------------

                    // Get the next words
                    nextWords.clear();

                    // Add the (n-1) words that come after the one being analysed
                    for (j = i + 1; j < taggedWordList.size() && (j - (i + 1)) < (n - 1); j++)
                    {
                        // The query for a "." is very slow and since it has no importance I'm removing it
                        if (taggedWordList.get(j).word().equals("."))
                            break;

                        // The query matches a symbol
                        if (!taggedWordList.get(j).word().matches("([a-zA-Z'])+|[.,!;:]"))
                            break;

                        // Replace the numbers with <number>
                        if (noSpecificNumbers)
                            nextWords.add(taggedWordList.get(j).word().replaceAll("-?[0-9]+([.,][0-9]+)?", "<number>"));
                        else
                            nextWords.add(taggedWordList.get(j).word());
                    }

                    // If the next words aren't enough
                    if (includeBeginEndTags)
                        if (nextWords.size() < n - 1)
                        {
                            // Adds strings that represent the end of the sentence
                            for (int k = n - 2; nextWords.size() < n - 1; k--)
                            {
                                if (k == 0)
                                {
                                    // Ads the last string
                                    nextWords.add("<stringn>");

                                    break;
                                }

                                nextWords.add("<stringn-" + k + ">");
                            }
                        }

                    /*System.out.println("Original Expression: " + ngram + " " + word + " " + nextWords);
                    System.out.println("Probability Original Expression: " + db.getProbabilityExpression2(ngram, word, nextWords, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated));*/

                    // Copy the list
                    // Used because of the removes
                    List<String> ngramPrevious = new ArrayList<>();
                    ngramPrevious.addAll(ngram);

                    // Copy the list
                    // Used because of the removes
                    List<String> ngramNext = new ArrayList<>();
                    ngramNext.addAll(nextWords);

                    List<String> next = new ArrayList<>();

                    int type = SuggestedReplacement.AFTER;

                    // Works with all the previous words, word of interest and next words to form an expression and calculate its probability
                    // by splitting into n-grams and adding the log of the probability of each
                    while (!ngramNext.isEmpty())
                    {
                        sug.addReplacementsFromType(db.getMostLikelyWordsOfInterest(ngramPrevious, next, null, (byte) 3, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated), type);

                        type++;

                        // Update the variables for the next n-gram
                        ngramPrevious.remove(0);
                        next.add(ngramNext.remove(0));
                    }

                    sug.addReplacementsFromType(db.getMostLikelyWordsOfInterest(ngramPrevious, next, null, (byte) 3, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated), type);

                    if (verbose)
                        System.out.println("\nCandidate list: " + sug.getReplacements() + "\n");

                    // Synonyms
                    // -------------------------------------------------------------------------

                    // If there is a POS of interest
                    if (wordTag != null)
                    {
                        // TODO: VERBS NEED TO BE STEMMED AND THEN THE SYNONYMS NEED TO BE CONJUGATED IN THE SAME MANNER AS THE ORIGINAL ONE
                        // TODO: SAME FOR NOUNS AND SINGULAR/PLURAL CASES
                        synonyms = wordnet.getSynonyms(word, wordTag);

                        conjSynonyms = new ArrayList<>();

                        if (synonyms != null)
                            // Conjugate the synonyms
                            for (String s : synonyms)
                            {
                                nlg.setWord(s, t.tag());
                                conjSynonyms.addAll(nlg.getVariants());

                                conjSynonyms.add(s);
                            }

                        if (verbose)
                            System.out.println("\nSynonyms: " + conjSynonyms);

                        sug.addReplacementsFromType(db.getMostLikelyNextWords(ngram, conjSynonyms, (byte) 3, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated), SuggestedReplacement.SYNONYMS);
                    }


                    // Prepositions
                    // -------------------------------------------------------------------------

                    if (t.tag().equals("IN"))
                        sug.addReplacementsFromType(Arrays.asList(CalculateSuggestions.prepositions), SuggestedReplacement.PREPOSITIONS);

                    // Final Analysis
                    // -------------------------------------------------------------------------

                    if (verbose)
                        System.out.println("\nFinal Candidates:" + sug.getReplacements() + "\n");

                    if (!sug.getReplacements().isEmpty())
                    {
                        // Add the current word if it's not on the list
                        if (!sug.getReplacements().contains(new SuggestedReplacement(SuggestedReplacement.NONE, word)))
                            sug.getReplacements().add(new SuggestedReplacement(SuggestedReplacement.NONE, word));

                        // If there's only 1 suggestion then it will be the word
                        if (replacementSuggestions.size() == 1)
                        {
                            // Remove the first element from the list
                            // To continue building with the rest of the words
                            ngram.remove(0);

                            // Add this word to the n-gram
                            ngram.add(word);

                            continue;
                        }

                        replacementSuggestions = db.getMostLikelyExpressions(ngram, sug.getReplacements(), nextWords, 3, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated);

                        if (replacementSuggestions != null)
                        {
                            if (!replacementSuggestions.isEmpty())
                            {
                                // This removes suggestions when the most likely suggestion is the current word and acceptSecondarySuggestions is false
                                if (!replacementSuggestions.get(0).equals(new SuggestedReplacement(SuggestedReplacement.NONE, word)) || acceptSecondarySuggestions)
                                {
                                    sug = new Suggestion(t.beginPosition(), t.endPosition());
                                    sug.getReplacements().addAll(replacementSuggestions);

                                    if (verbose)
                                        System.out.println("Chosen Candidates: " + replacementSuggestions);

                                    sent.getSuggestions().add(sug);
                                }
                            }
                        }
                    }

                    // Remove the first element from the list
                    // To continue building with the rest of the words
                    ngram.remove(0);
                }

                // Add this word to the n-gram
                ngram.add(word);
            }
        }
    }

    /**
     * Calculate all the possible suggestions for the text.
     * These are based on the synonyms of each word obtained from the wordnet, the n-grams and the (n-1) words that come after.
     *
     * @param text                the text to be analysed.
     * @param noSpecificNumbers   true if you want all numbers to be replaced by a special tag.
     * @param includeBeginEndTags true if it should include the special tags at the beginning and end.
     *
     * @return the list of sentences that resulted from the parsing of the text.
     */
    public List<Sentence> allSuggestions(String text, boolean noSpecificNumbers, boolean includeBeginEndTags)
    {
        if (text == null)
            return null;

        List<Sentence> sentences = new ArrayList<>();

        sentenceIterator.setText(text);
        int start = sentenceIterator.first();

        Sentence sent;

        // Iterate over the sentences of the text
        for (int end = sentenceIterator.next();
             end != BreakIterator.DONE;
             start = end, end = sentenceIterator.next())
        {
            sent = new Sentence(text.substring(start, end), start, end, true);

            sentences.add(sent);
        }

        allSuggestions(sentences, noSpecificNumbers, includeBeginEndTags);

        return sentences;
    }

    /**
     * Calculate all the possible suggestions for the text.
     * These are based on the synonyms of each word obtained from the wordnet, the n-grams and the (n-1) words that come after.
     *
     * @param sentences           the sentences to be analyzed.
     * @param noSpecificNumbers   true if you want all numbers to be replaced by a special tag.
     * @param includeBeginEndTags true if it should include the special tags at the beginning and end.
     */
    public void allSuggestions(List<Sentence> sentences, boolean noSpecificNumbers, boolean includeBeginEndTags)
    {
        if (sentences == null)
            return;

        Suggestion newSuggestion;

        // TODO: This value is the maximum currently at the DB
        short n = 3;
        int i, j;

        List<String> ngram = new ArrayList<>();
        List<String> synonyms, conjSynonyms;
        List<String> nextWords = new ArrayList<>();

        List<TaggedWord> taggedWordList;
        TaggedWord t;

        POS wordTag;

        List<String> x;

        SimpleNLG nlg = new SimpleNLG(verbose);

        // Iterate over the sentences of the text
        for (Sentence sent : sentences)
        {
            // If this sentence remains the same
            if (!sent.wasEdited())
                continue;

            sent.setWasEdited(false);

            // Add the suggestions from the language tool to the list of suggestions
            addRuleMatchSuggestionList(sent);

            // Parse the sentence with the Stanford Parser
            taggedWordList = sp.parseSentence(sent.getSentence());

            if (taggedWordList == null)
                continue;

            ngram.clear();

            // Adds strings that represent the beginning of the sentence
            if (includeBeginEndTags)
                for (i = 1; i < n; i++)
                    ngram.add("<string" + i + ">");

            // Iterate over all the words in the text string
            for (i = 0; i < taggedWordList.size(); i++)
            {
                t = taggedWordList.get(i);

                newSuggestion = new Suggestion(t.beginPosition(), t.endPosition());

                String word = t.word();

                // Replace the numbers with <number>
                if (noSpecificNumbers)
                    word = word.replaceAll("-?[0-9]+([.,][0-9]+)?", "<number>");

                // The query for a "." is very slow and since it has no importance I'm removing it
                if (word.equals("."))
                {
                    ngram.clear();
                    continue;
                }

                // TODO: NUMBERS DON'T GET SUGGESTIONS
                if (word.equals("<number>"))
                {
                    if (ngram.size() == (n - 1))
                        // Remove the first element from the list
                        // To continue building with the rest of the words
                        ngram.remove(0);

                    // Add this word to the n-gram
                    ngram.add(word);

                    continue;
                }

                // Skip non word or punctuation
                if (!word.matches("([a-zA-Z'`\"])+|[.,!;:]"))
                {
                    ngram.clear();
                    continue;
                }

                // If the n-gram has the correct size
                if (ngram.size() == (n - 1))
                {
                    // Get the POS of the word in the sentence
                    wordTag = sp.getPOSWordnet(t.tag());

                    if (verbose)
                        System.out.println("\nDB Queries:\n------------------------\nWord of interest: " + word + "\n");

                    // Synonyms
                    // -------------------------------------------------------------------------

                    // If there is a POS of interest
                    if (wordTag != null)
                    {
                        // TODO: VERBS NEED TO BE STEMMED AND THEN THE SYNONYMS NEED TO BE CONJUGATED IN THE SAME MANNER AS THE ORIGINAL ONE
                        // TODO: SAME FOR NOUNS AND SINGULAR/PLURAL CASES
                        synonyms = wordnet.getSynonyms(word, wordTag);

                        conjSynonyms = new ArrayList<>();

                        if (synonyms != null)
                            // Conjugate the synonyms
                            for (String s : synonyms)
                            {
                                nlg.setWord(s, t.tag());
                                conjSynonyms.addAll(nlg.getVariants());

                                conjSynonyms.add(s);
                            }

                        newSuggestion.addReplacementsFromType(conjSynonyms, SuggestedReplacement.SYNONYMS);
                    }

                    // Prepositions
                    // -------------------------------------------------------------------------

                    if (t.tag().equals("IN"))
                        newSuggestion.addReplacementsFromType(Arrays.asList(CalculateSuggestions.prepositions), SuggestedReplacement.PREPOSITIONS);

                    // Context suggestions
                    // -------------------------------------------------------------------------

                    // Get the next words
                    nextWords.clear();

                    // Add the (n-1) words that come after the one being analysed
                    for (j = i + 1; j < taggedWordList.size() && (j - (i + 1)) < (n - 1); j++)
                    {
                        // The query for a "." is very slow and since it has no importance I'm removing it
                        if (taggedWordList.get(j).word().equals("."))
                            break;

                        // The query matches a symbol
                        if (!taggedWordList.get(j).word().matches("([a-zA-Z'])+|[.,!;:]"))
                            break;

                        // Replace the numbers with <number>
                        if (noSpecificNumbers)
                            nextWords.add(taggedWordList.get(j).word().replaceAll("-?[0-9]+([.,][0-9]+)?", "<number>"));
                        else
                            nextWords.add(taggedWordList.get(j).word());
                    }

                    // If the next words aren't enough
                    if (includeBeginEndTags)
                        if (nextWords.size() < n - 1)
                        {
                            // Adds strings that represent the end of the sentence
                            for (int k = n - 2; nextWords.size() < n - 1; k--)
                            {
                                if (k == 0)
                                {
                                    // Ads the last string
                                    nextWords.add("<stringn>");

                                    break;
                                }

                                nextWords.add("<stringn-" + k + ">");
                            }
                        }

                    // Copy the list
                    // Used because of the removes
                    List<String> ngramPrevious = new ArrayList<>();
                    ngramPrevious.addAll(ngram);

                    // Copy the list
                    // Used because of the removes
                    List<String> ngramNext = new ArrayList<>();
                    ngramNext.addAll(nextWords);

                    List<String> next = new ArrayList<>();

                    int type = SuggestedReplacement.AFTER;

                    // Works with all the previous words, word of interest and next words to form an expression and calculate its probability
                    // by splitting into n-grams and adding the log of the probability of each
                    while (!ngramNext.isEmpty())
                    {
                        x = db.getMostLikelyWordsOfInterest(ngramPrevious, next, null, (byte) 10, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated);

                        if (x != null)
                            newSuggestion.addReplacementsFromType(x, type);

                        type++;

                        // Update the variables for the next n-gram
                        ngramPrevious.remove(0);
                        next.add(ngramNext.remove(0));
                    }

                    x = db.getMostLikelyWordsOfInterest(ngramPrevious, next, null, (byte) 10, document.getClosestDocuments(), suggestionTableName, suggestionFileSeparated);

                    if (x != null)
                        newSuggestion.addReplacementsFromType(x, type);

                    if (!newSuggestion.getReplacements().isEmpty())
                    {
                        sent.getSuggestions().add(newSuggestion);

                        if (verbose)
                            System.out.println(newSuggestion);
                    }

                    // Remove the first element from the list
                    // To continue building with the rest of the words
                    ngram.remove(0);
                }

                // Add this word to the n-gram
                ngram.add(word);
            }
        }
    }
}