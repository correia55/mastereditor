package auxiliary;

import java.util.List;

/**
 * Interface that represents a context of a document.
 *
 * Created by Acácio Correia on 12/06/2016.
 *
 * @author Acácio Correia
 */
public interface Context
{
    List<Integer> getClosestDocuments();
}