package auxiliary;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a document.
 *
 * Created by Acácio Correia on 12/06/2016.
 *
 * @author Acácio Correia
 */
public class Document
{
    private Context context;
    private List<Sentence> sentences;

    public Document()
    {
        sentences = new ArrayList<>();
    }

    public List<Sentence> getSentences()
    {
        return sentences;
    }

    public void setContext(Context context)
    {
        this.context = context;
    }

    public List<Integer> getClosestDocuments()
    {
        if(context == null)
            return null;

        return context.getClosestDocuments();
    }
}