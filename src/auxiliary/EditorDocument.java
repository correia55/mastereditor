package auxiliary;

import javafx.scene.control.TextArea;

import java.util.Stack;

/**
 * Represents a document in the editor, incrementing a document with associated variables from the editor.
 *
 * Created by Acácio Correia on 12/06/2016.
 *
 * @author Acácio Correia
 */
public class EditorDocument extends Document
{
    private TextArea textA;

    private Stack<TextChange> undoStack;
    private Stack<TextChange> redoStack;

    public EditorDocument(TextArea t)
    {
        super();

        textA = t;

        undoStack = new Stack<>();
        redoStack = new Stack<>();
    }

    public TextArea getTextA()
    {
        return textA;
    }

    public Stack<TextChange> getUndoStack()
    {
        return undoStack;
    }

    public Stack<TextChange> getRedoStack()
    {
        return redoStack;
    }

    public String getText()
    {
        return textA.getText();
    }

    public void setText(String text)
    {
        textA.setText(text);
    }
}