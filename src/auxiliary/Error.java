package auxiliary;

/**
 * Represents an error in the text, containing the correction.
 *
 * Created by Acácio Correia on 22/05/2016.
 *
 * @author Acácio Correia
 */
public class Error
{
    private int type;

    private TextChange change;

    public Error(int type)
    {
        this.type = type;
    }

    public Error(int type, TextChange change)
    {
        this.type = type;
        this.change = change;
    }

    public int getType()
    {
        return type;
    }

    public TextChange getChange()
    {
        return change;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public void setChange(TextChange change)
    {
        this.change = change;
    }

    @Override
    public String toString()
    {
        return "Error{" +
                "type=" + type +
                ", change=" + change +
                '}';
    }
}