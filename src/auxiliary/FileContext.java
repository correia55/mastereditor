package auxiliary;

import java.util.List;

/**
 * Represents the context of a document in the form of its closest documents.
 *
 * Created by acaci on 12/06/2016.
 *
 * @author Acácio Correia
 */
public class FileContext implements Context
{
    private List<Integer> closestDocuments;

    public FileContext(List<Integer> documents)
    {
        closestDocuments = documents;
    }

    public List<Integer> getClosestDocuments()
    {
        return closestDocuments;
    }
}