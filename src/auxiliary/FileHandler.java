package auxiliary;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.*;
import java.nio.file.Files;

/**
 * Reading and writing pdf and txt files.
 * It also allows for the file to be a directory, iterating over all the files in that directory.
 *
 * Source for parts of the code from SANN3:
 * http://stackoverflow.com/questions/18098400/how-to-get-raw-text-from-pdf-file-using-java
 *
 * @author Acácio Correia
 */
public class FileHandler
{
    public static void main(String[] args)
    {
        try
        {
            String pdfFilePath;

            // Read input from console
            try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in)))
            {
                System.out.println("Enter File Path or Directory:");
                pdfFilePath = br.readLine();
            }

            // It can either be a directory or a single file     
            File pdfFile = new File(pdfFilePath);
/*
            String fileout = pdfFile.getPath() + " - text";

            createNewFolderPDFToTextFile(pdfFile, fileout);

            // The location of the texts
            pdfFile = new File(pdfFile.getPath() + " - text");

            splitTrainTest(pdfFile, 0.8f, true);*/

            // If the file is a directory
            // Get all files to a single text file
            try (PrintWriter pwOut = new PrintWriter(new BufferedWriter(new FileWriter(pdfFilePath + ".txt", true))))
            {
                copyFile(pdfFile, pwOut, true, true);

                pwOut.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Splits the files in a directory into two folders: Train and test, following the given percentage.
     *
     * @param file            the directory file.
     * @param trainPercentage the percentage of files that are moved to the train folder.
     * @param isTrain         true if the file is a train file. Used with recursion.
     */
    public static void splitTrainTest(File file, float trainPercentage, boolean isTrain)
    {
        if (file.isDirectory())
        {
            File[] listOfFiles = file.listFiles();

            if (listOfFiles != null)
            {
                int numberFiles = (int) (listOfFiles.length * trainPercentage);
                int numberTrain = 0;

                for (File singleFile : listOfFiles)
                {
                    if (!singleFile.isDirectory())
                        numberTrain++;

                    splitTrainTest(singleFile, trainPercentage, numberTrain < numberFiles);
                }

            }
        }
        else
        {
            String fileout;

            if (isTrain)
            {
                fileout = file.getParent() + "\\Train";

                // Make the directory that will receive the text files
                new File(fileout).mkdir();
            }
            else
            {
                fileout = file.getParent() + "\\Test";

                // Make the directory that will receive the text files
                new File(fileout).mkdir();
            }

            try
            {
                // Move the file to the correct folder
                Files.move(file.toPath(), new File(fileout + "\\" + file.getName()).toPath());
            }
            catch (IOException e)
            {
                System.err.println("ERROR in copyFile");
                e.printStackTrace();
            }
        }
    }

    /**
     * Converts all pdf files in a directory into a new folder, in text.
     *
     * @param file    the file or directory into a text file.
     * @param fileout the path to the location of the generated file.
     */

    public static void createNewFolderPDFToTextFile(File file, String fileout)
    {
        // Make the directory that will receive the text files
        new File(fileout).mkdirs();

        // Add the next directory
        fileout += "\\" + file.getName();

        if (file.isDirectory())
        {
            File[] listOfFiles = file.listFiles();

            if (listOfFiles != null)
                for (File singleFile : listOfFiles)
                    createNewFolderPDFToTextFile(singleFile, fileout);
        }
        else
        {
            fileout += ".txt";

            PrintWriter pwOut;

            try
            {
                String result = readPDFText(file);

                if (result != null)
                {
                    pwOut = new PrintWriter(new BufferedWriter(new FileWriter(fileout, false)));
                    pwOut.write(result);
                    pwOut.close();
                }
            }
            catch (IOException e)
            {
                System.err.println("ERROR in createNewFolderPDFToTextFile");
                e.printStackTrace();
            }
        }
    }

    /**
     * Reads a text or pdf file then saves it to a new file.
     * The text can be written as is or as a single line in an existing file.
     * It also allows for the file to be a directory, iterating over all the files in that directory.
     *
     * @param file             the file or directory into a text file.
     * @param pwOut            the PrintWriter for the output file.
     * @param fileAsLine       True if the file should be converted to a single line.
     * @param removeSmallLines True if you want to remove lines with less that 15 characters in the file.
     */
    public static void copyFile(File file, PrintWriter pwOut, boolean fileAsLine, boolean removeSmallLines)
    {
        if (file.isDirectory())
        {
            if (file.getName().equals("Train"))
                return;

            if (file.getName().equals("Test"))
                return;

            File[] listOfFiles = file.listFiles();

            if (listOfFiles != null)
                for (File singleFile : listOfFiles)
                    copyFile(singleFile, pwOut, fileAsLine, removeSmallLines);
        }
        else
        {
            String result;
            boolean isPDF = file.getName().matches("(?m)\\.pdf$");

            String resultingFile = file.getAbsolutePath();

            // Ends in .pdf
            if (isPDF)
            {
                result = readPDFText(file);
                resultingFile += ".txt";
            }
            else
                result = readTXTText(file);

            // Remove lines with less that 15 characters
            if (removeSmallLines)
                result = result.replaceAll("(?m)^.{0,15}\n", "");

            if (fileAsLine)
                writeWordsAsLine(result, file.getName(), pwOut);
            else
            {
                try
                {
                    if (result != null)
                    {
                        pwOut = new PrintWriter(new BufferedWriter(new FileWriter(resultingFile, false)));
                        pwOut.write(result);
                        pwOut.close();
                    }
                }
                catch (IOException e)
                {
                    System.err.println("ERROR in copyFile");
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Reads a pdf file and returns the text.
     *
     * @param pdf the pdf file to read.
     */
    public static String readPDFText(File pdf)
    {
        // Check if the file has a pdf extension
        if (!pdf.getPath().endsWith(".pdf"))
        {
            System.err.println(pdf.getAbsolutePath() + " does not have a pdf extension.");
            return null;
        }

        PDDocument pdDoc = null;
        COSDocument cosDoc = null;

        String parsedText = null;

        try
        {
            // Create the PDF parser
            PDFParser parser = new PDFParser(new RandomAccessFile(pdf, "r"));

            // Parse the pdf file
            parser.parse();
            cosDoc = parser.getDocument();

            // Get the parsed text
            PDFTextStripper pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);

            parsedText = pdfStripper.getText(pdDoc);
        }
        catch (Exception e)
        {
            System.err.println("ERROR in readPDFText: " + pdf);
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (cosDoc != null)
                    cosDoc.close();

                if (pdDoc != null)
                    pdDoc.close();
            }
            catch (Exception e1)
            {
                e1.printStackTrace(System.err);
                System.exit(1);
            }
        }

        return parsedText;
    }

    /**
     * Read the text from a text file.
     *
     * @param file the text file of interest.
     *
     * @return the text from the file.
     */
    public static String readTXTText(File file)
    {
        String text = "";

        // Read the text from the file
        try (BufferedReader br = new BufferedReader(new FileReader(file)))
        {
            String line = br.readLine();

            while (line != null)
            {
                text = text.concat(line + "\n");

                line = br.readLine();
            }

            br.close();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: Reading file - " + file);
            e.printStackTrace();
        }

        return text;
    }

    /**
     * Writes a string into a line of a text file, removing all characters non alphabetic.
     *
     * @param parsedText the text to write.
     * @param fileName   the name of the file.
     * @param textFile   the PrintWriter for the output file.
     */
    private static void writeWordsAsLine(String parsedText, String fileName, PrintWriter textFile)
    {
        if (parsedText == null)
            return;

        //textFile.println(parsedText.replaceAll("[^A-Za-z0-9. ]+", ""));
        //textFile.println(CharMatcher.JAVA_ISO_CONTROL.removeFrom(parsedText));

        // Removes newlines for spaces
        // Removes all characters that are not text
        textFile.println(fileName + ", X, " + parsedText.replaceAll("[\\t\\n\\r—]", " ").replaceAll("[^A-Za-z 0-9]", ""));
    }
}