package auxiliary;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.item.*;
import edu.mit.jwi.morph.WordnetStemmer;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Based on the manual of the JWI.
 *
 * @author Acácio Correia
 */
public class JWI
{
    // The verbosity of the output
    private final boolean verbose;

    private IDictionary dict;

    public static void main(String[] args)
    {
        JWI jwi = new JWI(true);

        // Load the dictionary to memory
        //jwi.loadDictMemory(dict);
        // Stem a word
        //jwi.wordnetStemming(dict, "dogs", POS.NOUN);
        //jwi.wordMeaning("test", POS.NOUN);
        // iterate over words associated with the synset
        jwi.getSynonyms("skill", POS.NOUN).forEach(System.out::println);
        //jwi.getHypernyms("test", POS.NOUN);
    }

    public JWI(boolean verbose)
    {
        this(verbose, "D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\dict");
    }

    public JWI(boolean verbose, String path)
    {
        this.verbose = verbose;

        try
        {
            URL url = new URL("file", null, path);

            // Construct the dictionary object and open it
            dict = new Dictionary(url);
            dict.open();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: " + e);
        }
    }

    /**
     * Get the list of synonyms for all the stems of this word.
     *
     * @param word to be stemmed.
     * @param pos  the part-of-speech tag.
     *
     * @return the list of synonyms of this word.
     */
    public List<String> getSynonyms(String word, POS pos)
    {
        String stem = wordnetStemming(word, pos);

        // If it finds no stem for this word with that POS
        if (stem == null)
            return null;

        IIndexWord idxWord = dict.getIndexWord(stem, pos);

        if (idxWord == null)
            return null;

        List<String> synonyms = new ArrayList<>();

        IWordID wordID;
        IWord iWord;
        ISynset synset;

        // TODO: HOW TO SELECT THE SYNSET???
        // Now we're looking for the synonyms of all of the synsets of a word
        for (byte i = 0; i < idxWord.getWordIDs().size(); i++)
        {
            wordID = idxWord.getWordIDs().get(i);

            iWord = dict.getWord(wordID);
            synset = iWord.getSynset();

            // Add each synonym to the list
            synset.getWords().forEach((w) ->
            {
                // Prevent the insertion of repetitions
                // Replaces the '_' which comes in the multi word expressions with a ' '
                if (!synonyms.contains(w.getLemma()))
                    synonyms.add(w.getLemma().replace("_", " "));
            });
        }

        return synonyms;
    }

    /**
     * Get the list of hipernyms of this word.
     * Automatically stems the word.
     *
     * @param word to be stemmed.
     * @param pos  the part-of-speech tag.
     */
    public void getHypernyms(String word, POS pos)
    {
        // Get the synset
        IIndexWord idxWord = dict.getIndexWord(word, pos);
        IWordID wordID = idxWord.getWordIDs().get(0); // 1st meaning
        IWord iWord = dict.getWord(wordID);
        ISynset synset = iWord.getSynset();

        // Get the hypernyms
        List<ISynsetID> hypernyms = synset.getRelatedSynsets(Pointer.HYPERNYM);

        // Print out each hypernyms id and synonyms
        List<IWord> words;

        for (ISynsetID sid : hypernyms)
        {
            words = dict.getSynset(sid).getWords();
            System.out.print(sid + " {");

            for (Iterator<IWord> i = words.iterator(); i.hasNext(); )
            {
                System.out.print(i.next().getLemma());
                if (i.hasNext())
                {
                    System.out.print(", ");
                }
            }

            System.out.println("}");
        }
    }

    /**
     * Get the first meaning of a word.
     *
     * @param word the word of interest.
     * @param pos  the POS tag for the word.
     */
    public void wordMeaning(String word, POS pos)
    {
        // look up first sense of the word "dog "
        IIndexWord idxWord = dict.getIndexWord(word, pos);
        IWordID wordID = idxWord.getWordIDs().get(0);
        IWord iWord = dict.getWord(wordID);

        System.out.println("Id = " + wordID);
        System.out.println("Lemma = " + iWord.getLemma());
        System.out.println("Gloss = " + iWord.getSynset().getGloss());
    }

    /**
     * Return the first possible stem for a word,
     * according to the POS tag. This returns the word itself when no stem is found.
     *
     * @param word to be stemmed.
     * @param pos  the part-of-speech tag.
     *
     * @return the result of stemming of a word.
     */
    // TODO: HOW TO CHOSE THE STEM???
    public String wordnetStemming(String word, POS pos)
    {
        WordnetStemmer ws = new WordnetStemmer(dict);

        try
        {
            List<String> stems = ws.findStems(word, pos);

            int numStems = stems.size();

            if (numStems == 0)
            {
                if (verbose)
                    System.err.println("WARNING: No stems were found for " + word + ".");

                return word;
            }
            else if (numStems > 1)
                if (verbose)
                    System.err.println("WARNING: There were more stems for the word " + word + ".");

            return stems.get(0);
        }
        catch (IllegalArgumentException e)
        {
            return word;
        }
    }

    public void loadDictMemory()
    {
        try
        {
            IRAMDictionary rDict = new RAMDictionary(dict, ILoadPolicy.NO_LOAD);
            rDict.open();

            // do something slowly
            trek(rDict);

            // now load into memory
            System.out.print("\nLoading Wordnet into memory ...");
            long t = System.currentTimeMillis();
            rDict.load(true);
            System.out.printf(" done (%d msec)\n", System.currentTimeMillis() - t);

            // do the same thing again , only faster
            trek(rDict);
        }
        catch (IOException | InterruptedException e)
        {
            System.err.println("ERROR: " + e);
        }
    }

    public void trek(IDictionary dict)
    {
        int tickNext = 0;
        int tickSize = 20000;
        int seen = 0;

        System.out.print(" Treking across Wordnet ");
        long t = System.currentTimeMillis();
        for (POS pos : POS.values())
        {
            for (Iterator<IIndexWord> i = dict.getIndexWordIterator(pos); i.hasNext(); )
            {
                for (IWordID wid : i.next().getWordIDs())
                {
                    seen += dict.getWord(wid).getSynset().getWords().size();

                    if (seen > tickNext)
                    {
                        System.out.print('.');
                        tickNext = seen + tickSize;
                    }
                }
            }
        }

        System.out.printf(" done (%d msec)\n", System.currentTimeMillis() - t);
        System.out.println("In my trek I saw " + seen + " words ");
    }
}