package auxiliary;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.MarginalProbEstimator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Based on the manual from MALLET.
 *
 * @author Acácio Correia
 */
public class LDA
{
    private ParallelTopicModel ldaModel;

    private boolean verbose;

    public PrintWriter logFile;

    public static void main(String[] args) throws Exception
    {
        LDA lda = new LDA(true);

        lda.logFile.println("\nPROCESS START\n----------------");

        for (int i = 1; i <= 25; i++)
        {
            lda.estimateModelCorpus(new File("D:\\Desktop\\arxiv\\cs - 5319 - train.txt"), "csCorpusState" + (50*i) + ".txt", 50 * i, true);

            //lda.importSavedState("D:\\Desktop\\arxiv\\csCorpusState.txt");
            lda.logLikelihoodEstimation("D:\\Desktop\\arxiv\\cs - 5319 - validation.txt");

            lda.logFile.flush();
        }

        lda.logFile.close();

        /*lda.importSavedState("D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\corpusState.txt");

        double[] topicDistribution = lda.distributionNewDocument(FileHandler.readTXTText(new File("D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\aclArc\\A00-1001.txt")));
        //double[] topicDistribution = lda.distributionNewDocument(FileHandler.readTXTText(new File("D:\\Desktop\\rando_chang_CELL_2012.pdf.txt")));


        List<Integer> closestDocuments;

        //closestDocuments = lda.getDocumentsUnderThreshold(topicDistribution, 3f);
        closestDocuments = lda.getClosestDocuments(topicDistribution, 100);

        System.out.println("Number of Documents: " + closestDocuments.size());

        System.out.println(closestDocuments);*/
    }

    public LDA(boolean verbose)
    {
        this.verbose = verbose;

        try
        {
            logFile = new PrintWriter(new BufferedWriter(new FileWriter("lda.log", true)));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private List<Pipe> createPipeList()
    {
        List<Pipe> pipeList = new ArrayList<>();

        // Normalize all tokens to all lowercase
        pipeList.add(new CharSequenceLowercase());

        // Tokenize raw strings
        pipeList.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));

        // Remove stopwords from a standard English stoplist.
        pipeList.add(new TokenSequenceRemoveStopwords(new File("resources\\stop-list_en.txt"), "UTF-8", false, false, false));

        // Rather than storing tokens as strings, convert
        // them to integers by looking them up in an alphabet.
        pipeList.add(new TokenSequence2FeatureSequence());

        // Do the same thing for the "target" field:
        // convert a class label string to a Label object,
        // which has an index in a Label alphabet.
        //pipeList.add(new Target2Label());

        // Now convert the sequence of features to a sparse vector,
        // mapping feature IDs to counts.
        //pipeList.add(new FeatureSequence2FeatureVector());

        return pipeList;
    }

    /**
     * Estimate the latent structure of the corpus in the given file.
     *
     * @param corpusTextFile the file with the corpus written in the format that
     *                       each document is in a line.
     * @param stateFileName  name of the file that will store the probability distributions calculated.
     * @param saveState      True if the state should be saved to file
     */
    public void estimateModelCorpus(File corpusTextFile, String stateFileName, int numberTopics, boolean saveState)
    {
        try
        {
            // Begin by importing documents from text to feature sequences
            List<Pipe> pipeList = createPipeList();

            InstanceList instances = new InstanceList(new SerialPipes(pipeList));

            // To read the corpus from the file
            // Each document is an instance
            Reader fileReader = new InputStreamReader(new FileInputStream(corpusTextFile), "UTF-8");
            instances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields

            // This is LDA
            // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
            // Note that the first parameter is passed as the sum over topics, while
            // the second is the parameter for a single dimension of the Dirichlet prior.
            ldaModel = new ParallelTopicModel(numberTopics, 50, 0.1);

            ldaModel.addInstances(instances);

            // Use two parallel samplers, which each look at one half the corpus and combine
            // statistics after every iteration.
            ldaModel.setNumThreads(2);

            // Run the model for 50 iterations and stop (this is for testing only,
            // for real applications, use 1000 to 2000 iterations)
            int numIterations = 1000;

            ldaModel.setNumIterations(numIterations);
            ldaModel.estimate();

            logFile.println("Num Types: " + ldaModel.numTypes);
            logFile.println("Total token: " + ldaModel.totalTokens);

            // Save the state of the model
            if (saveState)
                ldaModel.write(new File(corpusTextFile.getParent() + "\\" + stateFileName));

            logFile.println("File: " + corpusTextFile.getParent() + "\\" + stateFileName + "\n");
            logFile.println("Number of topics: " + numberTopics);

            // The number of documents
            System.out.println("Number of documents in the corpus: " + ldaModel.getData().size());
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Estimates the log likelihood of a model on a test file.
     *
     * @param testingTextFile the path to the test fule.
     */
    public void logLikelihoodEstimation(String testingTextFile)
    {
        try
        {
            // Begin by importing documents from text to feature sequences
            List<Pipe> pipeList = createPipeList();

            InstanceList testInstances = new InstanceList(new SerialPipes(pipeList));

            // To read the corpus from the file
            // Each document is an instance
            Reader fileReader = new InputStreamReader(new FileInputStream(testingTextFile), "UTF-8");
            testInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields

            MarginalProbEstimator evaluator = ldaModel.getProbEstimator();

            double logLikelihood = evaluator.evaluateLeftToRight(testInstances, 10, false, null);

            logFile.println("Log Likelihood: " + logLikelihood);
        }
        catch (IOException e)
        {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Import a previously saved state of the model.
     *
     * @param stateDocument the document where the state has been saved.
     */
    public void importSavedState(String stateDocument)
    {
        try
        {
            ldaModel = ParallelTopicModel.read(new File(stateDocument));
        }
        catch (Exception e)
        {
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }

    /**
     * Calculate the distribution of topics for a file in a String.
     *
     * @param documentText the document in a String.
     *
     * @return the distribution of topics in the file.
     */
    public double[] distributionNewDocument(String documentText)
    {
        // Begin by importing documents from text to feature sequences
        List<Pipe> pipeList = createPipeList();

        InstanceList instances = new InstanceList(new SerialPipes(pipeList));

        instances.addThruPipe(new Instance(documentText, null, "New EditorDocument", null));

        // Topic Inference for new documents
        TopicInferencer ti = ldaModel.getInferencer();

        return ti.getSampledDistribution(instances.get(0), 10, 1, 5);
    }

    /**
     * Get the numberDocuments closest documents to the given distribution in
     * terms of topics.
     *
     * @param documentTopicDistribution the topic distribution for the document.
     * @param numberDocuments           the number of documents we are interested in.
     *
     * @return the positions of the documents closest to the document of
     * interest.
     */
    public List<Integer> getClosestDocuments(double[] documentTopicDistribution, int numberDocuments)
    {
        List<Integer> listDocuments = new ArrayList<>(numberDocuments);
        double[] listDistributions = new double[numberDocuments];

        double[] topicDistribution;

        double newDivergence;

        for (int i = 0; i < ldaModel.getData().size(); i++)
        {
            topicDistribution = ldaModel.getTopicProbabilities(i);

            newDivergence = symmetricKLDivergence(topicDistribution, documentTopicDistribution);

            // If there are available spaces in the list of documents
            if (i < numberDocuments)
            {
                listDocuments.add(i);
                listDistributions[i] = newDivergence;
            }
            else
            {
                // If this document is closer that the last one on the list
                if (newDivergence < listDistributions[numberDocuments - 1])
                {
                    listDistributions[numberDocuments - 1] = newDivergence;
                    listDocuments.set(numberDocuments - 1, i);
                }
                // It does not belong to the list
                else
                {
                    continue;
                }
            }

            // Place the new document on the correct position of the list
            for (int j = Math.min(i, numberDocuments - 1) - 1; j >= 0; j--)
            {
                if (newDivergence < listDistributions[j])
                {
                    double auxD = listDistributions[j + 1];
                    int aux = listDocuments.get(j + 1);

                    // Exchange the distributions
                    listDistributions[j + 1] = listDistributions[j];
                    listDistributions[j] = auxD;

                    // Exchange the documents
                    listDocuments.set(j + 1, listDocuments.get(j));
                    listDocuments.set(j, aux);
                }
                // It is in the correct position
                else
                {
                    break;
                }
            }
        }

        return listDocuments;
    }

    /**
     * Get all the documents with a topics distribution under the defined threshold.
     * The order in the KL-divergence matters.
     *
     * @param documentTopicDistribution the topic distribution for all of the documents.
     * @param threshold                 the defined threshold.
     *
     * @return the documents with a topics distribution under the defined threshold.
     */
    public List<Integer> getDocumentsUnderThreshold(double[] documentTopicDistribution, float threshold)
    {
        double[] topicDistribution;

        double newDivergence;

        List<Integer> documents = new ArrayList<>();

        if (verbose)
            System.out.println("\nClosest Documents:\n------------------------");

        for (int i = 0; i < ldaModel.getData().size(); i++)
        {
            topicDistribution = ldaModel.getTopicProbabilities(i);

            newDivergence = symmetricKLDivergence(topicDistribution, documentTopicDistribution);

            if (newDivergence < threshold)
            {
                if (verbose)
                {
                    System.out.println(i);
                }

                documents.add(i);
            }
        }

        return documents;
    }

    /**
     * Returns the KL divergence, K(p1 || p2).
     *
     * The log is w.r.t. base 2.
     * <p>
     *
     * *Note*: If any value in <tt>p2</tt> is <tt>0.0</tt> then the
     * KL-divergence is <tt>infinite</tt>. Limin changes it to zero instead of
     * infinite.
     *
     * http://www.java2s.com/Code/Java/Development-Class/ReturnstheKLdivergenceKp1p2.htm
     */
    public static double klDivergence(double[] p1, double[] p2)
    {
        double klDiv = 0.0;

        for (int i = 0; i < p1.length; ++i)
        {
            if (p1[i] == 0)
            {
                continue;
            }
            if (p2[i] == 0.0)
            {
                continue;
            } // Limin

            klDiv += p1[i] * Math.log(p1[i] / p2[i]);
        }

        return klDiv / Math.log(2); // moved this division out of the loop -DM
    }

    /**
     * The symmetric version of KL divergence.
     *
     * @param p1 the fisrt vector.
     * @param p2 the second vector.
     * @return the result of symmetric KL divergence.
     */
    public static double symmetricKLDivergence(double[] p1, double[] p2)
    {
        return (klDivergence(p1, p2) + klDivergence(p2, p1))/2;
    }
}