package auxiliary;

import org.languagetool.JLanguageTool;
import org.languagetool.Language;
import org.languagetool.language.BritishEnglish;
import org.languagetool.rules.RuleMatch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Based on the examples.
 *
 * @author Acácio Correia
 */
public class LanguageTool
{
    JLanguageTool languageTool;

    public static void main(String[] args)
    {
        LanguageTool langTool = new LanguageTool();

        langTool.checkSentence("A sentence with a error in the Hitchhiker's Guide tot he Galaxy");
        langTool.checkSentence("Let us try som eerrors here!");
    }

    public LanguageTool()
    {
        this(new BritishEnglish());
    }

    public LanguageTool(Language language)
    {
        languageTool = new JLanguageTool(language);
    }

    /**
     * Splits a text into sentences.
     *
     * @param text the string with the complete text.
     *
     * @return a list with the sentences.
     */
    public List<String> tokenizeText(String text)
    {
        return languageTool.sentenceTokenize(text);
    }

    /**
     * Check a sentence for errors.
     *
     * @param sentence the string with the sentence.
     *
     * @return the suggestion list.
     */
    public List<RuleMatch> checkSentence(String sentence)
    {
        try
        {
            return languageTool.check(sentence);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Splits the text into sentences and checks each sentence.
     *
     * @param text the string with the text.
     *
     * @return the list of suggestions for all the text.
     */
    public List<RuleMatch> checkText(String text)
    {
        List<RuleMatch> suggestions = new ArrayList<>();

        tokenizeText(text).forEach((sentence) ->
                suggestions.addAll(checkSentence(sentence)));

        return suggestions;
    }
}