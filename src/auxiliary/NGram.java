package auxiliary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.*;

/**
 * The class to represent a n-gram.
 *
 * @author Acácio Correia
 */
public class NGram
{
    private final int ngramSize;

    private int wordCount;

    private final BreakIterator wordIterator;
    private final BreakIterator sentenceIterator;
    private final HashMap<List<String>, Integer> ngramCounts;

    public static void main(String[] args)
    {
        NGram ngram = new NGram(3);

        //ngram.readNgramCountsFile(new File("D:\\Desktop\\Tese\\Corpus\\w3_.txt"));
        ngram.calculateNgramCountsFile(new File("D:\\Desktop\\problems.txt"), true, true);

        ngram.printCounts();
    }

    public NGram(int n)
    {
        this.ngramSize = n;

        wordCount = 0;

        ngramCounts = new HashMap<>();
        wordIterator = BreakIterator.getWordInstance(new Locale("en", "GB"));
        sentenceIterator = BreakIterator.getSentenceInstance(new Locale("en", "GB"));
    }

    public int getWordCount()
    {
        return wordCount;
    }

    /**
     * Prints a n-gram and its count.
     *
     * @param ngram the n-gram.
     */
    public void printNgramCount(List<String> ngram)
    {
        String ngramString = "{";

        for (String s : ngram)
            ngramString += s + ", ";

        ngramString += "} = " + ngramCounts.get(ngram);

        System.out.println(ngramString);
    }

    /**
     * Print all the n-grams and respective counts.
     *
     * http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap
     * karim79
     */
    public void printCounts()
    {
        Iterator it = ngramCounts.entrySet().iterator();

        while (it.hasNext())
        {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            it.remove(); // avoids a ConcurrentModificationException
        }
    }

    /**
     * Updates the value of count for this n-gram.
     *
     * @param ngram the n-gram of interest.
     */
    private void updateNgramCount(List<String> ngram, int count)
    {
        ngramCounts.put(new ArrayList<>(ngram), count);
    }

    /**
     * Returns the count for this n-gram.
     *
     * @param ngram of interest.
     *
     * @return the count for this n-gram.
     */
    private int getNgramCount(List<String> ngram)
    {
        return (ngramCounts.get(ngram));
    }

    public HashMap<List<String>, Integer> getNgramCounts()
    {
        return ngramCounts;
    }

    /**
     * Reads n-gram counts from a file where:
     * - first position is the count;
     * - remaining positions form the n-gram.
     *
     * @param ngramFile the file.
     */
    private void readNgramCountsFile(File ngramFile)
    {
        try (BufferedReader br = new BufferedReader(new FileReader(ngramFile)))
        {
            String line = br.readLine();

            String[] lineParts;

            while (line != null)
            {
                lineParts = line.split("\\s+");

                List<String> ngram = new ArrayList<>();

                ngram.addAll(Arrays.asList(lineParts).subList(1, lineParts.length));

                updateNgramCount(ngram, Integer.parseInt(lineParts[0]));

                printNgramCount(ngram);

                line = br.readLine();
            }

            br.close();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: Reading file - " + e);
        }
    }

    /**
     * Splits the text into separate words.
     *
     * @param text the text of interest.
     *
     * @return the list of words.
     */
    private List<String> splitString(String text)
    {
        List<String> wordList = new ArrayList<>();

        wordIterator.setText(text);
        int start = wordIterator.first();

        for (int end = wordIterator.next();
             end != BreakIterator.DONE;
             start = end, end = wordIterator.next())
        {
            // Add the new word to the list
            wordList.add(text.substring(start, end));
        }

        return wordList;
    }

    // TODO: ADD STEMMING TO THIS

    /**
     * Updates the counts for the n-grams on the sentence.
     *
     * @param sentence          of interest.
     * @param noPunctuation     should exclude punctuation.
     * @param noSpecificNumbers should exclude specific numbers and use a general number tag instead.
     */
    private void sentenceNGramCount(String sentence, boolean noPunctuation, boolean noSpecificNumbers)
    {
        List<String> ngram = new ArrayList<>();

        String word;

        int count;

        // Adds strings that represent the beginning of the sentence
        for (int i = 1; i < ngramSize; i++)
            ngram.add("<string" + i + ">");

        wordIterator.setText(sentence);
        int start2 = wordIterator.first();

        boolean wasSymbol = false;

        // If it contains no words
        if(start2 == BreakIterator.DONE)
            return;

        // Iterate over the words of the sentence
        for (int end2 = wordIterator.next();
             end2 != BreakIterator.DONE;
             start2 = end2, end2 = wordIterator.next())
        {
            word = sentence.substring(start2, end2).toLowerCase();

            // Prevents empty words
            if (word.trim().isEmpty())
                continue;

            if(wasSymbol)
            {
                wasSymbol = false;
                ngram.clear();
            }

            if (noPunctuation)
            {
                if (word.matches("-?[0-9]+([.,][0-9]+)?") && noSpecificNumbers)
                {
                    word = "<number>";
                }
                else
                {
                    // If it doesn't contain at least a letter
                    //if(!word.matches(".*[a-zA-Z].*"))
                    // If it contains things other than letters
                    if (!word.matches("([a-zA-Z'`\"])+|[.,!;:]"))
                    {
                        wasSymbol = true;
                        continue;
                    }
                }
            }

            // Adds 1 to the word count
            wordCount++;

            // Add this word to the n-gram
            ngram.add(word);

            // If the n-gram has the correct size
            if (ngram.size() == ngramSize)
            {
                // Update the n-gram count
                count = ngramCounts.getOrDefault(ngram, 0) + 1;

                updateNgramCount(ngram, count);

                // Remove the first element from the list
                // To continue building with the rest of the words
                ngram.remove(0);
            }
        }

        // Adds strings that represent the end of the sentence
        for (int i = ngramSize - 2; i > 0; i--)
        {
            ngram.add("<stringn-" + i + ">");

            // Update the n-gram count
            count = ngramCounts.getOrDefault(ngram, 0) + 1;

            updateNgramCount(ngram, count);

            ngram.remove(0);
        }

        // Ads the last string
        ngram.add("<stringn>");

        // Update the n-gram count
        count = ngramCounts.getOrDefault(ngram, 0) + 1;

        updateNgramCount(ngram, count);
    }

    /**
     * Calculate the n-grams for a string of text, combining splitString and textNGramCount.
     *
     * @param text              the text to analyse.
     * @param noPunctuation     should exclude punctuation.
     * @param noSpecificNumbers should exclude specific numbers and use a general number tag instead.
     */
    public void calculateNgramCountsText(String text, boolean noPunctuation, boolean noSpecificNumbers)
    {
        sentenceIterator.setText(text);
        int start = sentenceIterator.first();

        // Iterate over the sentences of the text
        for (int end = sentenceIterator.next();
             end != BreakIterator.DONE;
             start = end, end = sentenceIterator.next())
        {
            sentenceNGramCount(text.substring(start, end), noPunctuation, noSpecificNumbers);
        }
    }

    /**
     * Calculate the n-grams for a text file.
     *
     * @param textFile the file of interest.
     */
    public void calculateNgramCountsFile(File textFile)
    {
        calculateNgramCountsFile(textFile, false, false);
    }

    /**
     * Calculate the n-grams for a text file, excluding punctuation.
     *
     * @param textFile          the file of interest.
     * @param noPunctuation     should exclude punctuation.
     * @param noSpecificNumbers should exclude specific numbers and use a general number tag instead.
     */
    public void calculateNgramCountsFile(File textFile, boolean noPunctuation, boolean noSpecificNumbers)
    {
        StringBuilder sb;

        try (BufferedReader br = new BufferedReader(new FileReader(textFile)))
        {
            sb = new StringBuilder();
            String line = br.readLine();

            // Read the complete file
            while (line != null)
            {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            br.close();
        }
        catch (IOException e)
        {
            System.err.println("ERROR: Reading file - " + e);
            return;
        }

        calculateNgramCountsText(sb.toString(), noPunctuation, noSpecificNumbers);
    }

    /**
     * Get the probability of this ngram.
     *
     * @param ngram the ngram of interest.
     *
     * @return the probability of this ngram.
     */
    public double getProbabilityNgram(List<String> ngram)
    {
        if (wordCount == 0)
            return 0;

        return getNgramCount(ngram) / wordCount;
    }
}