package auxiliary;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that interacts with the DB.
 *
 * @author Acácio Correia
 */
public class NGramDB
{
    private Connection dbConnection;
    private Statement stmtQuery;

    private final HashMap<String, Integer> sumCounts;
    private final HashMap<String, List<String>> contextResults;

    // The initial file in the files table
    private int initialFile;

    private boolean verbose;

    private boolean probabilityKN;

    public static void main(String[] args)
    {
        NGramDB ngram = new NGramDB(true);

        List<String> history = new ArrayList<>();
        history.add("test");
        history.add("this");

        //System.out.println(ngram.calculateGamma(history));

        System.out.println("KN: " + ngram.probabilityKN(history, "tool", null, "csTotal", false));

        history.add("test");
        history.add("this");

        System.out.println("Normal: " + ngram.getProbabilityNgram(history, "tool", null, "csTotal", false));


        //ngram.fillTableFromDirectory(new File("D:\\Desktop\\arxiv\\text\\cs - 5319 - text"), "cs1", "file_arxiv", "cs", 1, false);

        /*List<String> prevWord = new ArrayList<>();
        prevWord.add("in");
        prevWord.add("this");

        List<String> nextWord = new ArrayList<>();
        nextWord.add("is");
        nextWord.add("presented");

        List<Integer> files = new ArrayList<>();
        files.add(503);
        files.add(506);

        List<String> x = ngram.getMostLikelyWordsOfInterest(prevWord, null, null, (byte)3, files, "nopunctuation", true);

        if(x != null)
        {
            for(int i = 0; i < x.size(); i++)
                System.out.println(x.get(i));
        }*/

        //System.out.println(ngram.getProbabilityExpression2(prevWord, "paper", nextWord, null, "nopunctuation", true));

        ngram.closeDBConnection();
    }

    public NGramDB()
    {
        this(false);
    }

    public NGramDB(boolean verbose)
    {
        this(verbose, false);
    }

    public NGramDB(boolean verbose, boolean probabilityKN)
    {
        this("jdbc:mysql://localhost:3306/mastereditordb", //url
                "yourMaster",                              //username
                "123macaquinhodochines",                   //password
                verbose, probabilityKN);
    }

    public NGramDB(String url, String username, String password, boolean verbose, boolean probabilityKN)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            dbConnection = DriverManager.getConnection(url, username, password);
            stmtQuery = dbConnection.createStatement();
        }
        catch (ClassNotFoundException | SQLException e)
        {
            System.err.println("ERROR at NGramDB: ");
            e.printStackTrace();
            System.exit(1);
        }

        sumCounts = new HashMap<>();
        contextResults = new HashMap<>();

        this.verbose = verbose;
        this.probabilityKN = probabilityKN;
    }

    /**
     * Close the database connection.
     */
    public void closeDBConnection()
    {
        try
        {
            dbConnection.close();
        }
        catch (SQLException e)
        {
            System.err.println("ERROR at closeDBConnection: " + e);
        }
    }

    /**
     * Check if the parameteres are valid.
     *
     * @param words                 the list of words previous to the ones we want and the current subword.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param functionName          the name of the function that called this one.
     *
     * @return true if the parameteres are valid.
     */
    public boolean verifyParameteres(List<String> words, byte numberWordsInterested, String functionName)
    {
        if (words == null)
        {
            System.err.println("ERROR at " + functionName + ": Previous words is null.");
            return false;
        }

        if (words.size() > 4 || words.isEmpty())
        {
            System.err.println("ERROR at " + functionName + ": Not prepared for empty lists or lists with more than four elements.");
            return false;
        }

        if (numberWordsInterested == 0)
        {
            System.err.println("ERROR at " + functionName + ": number of words interested is 0.");
            return false;
        }

        return true;
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability, according to the list of previous words and part of the current word.
     *
     * @param words                 the list of words previous to the ones we want and the current subword.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the most probable word in a list, according to the list of previous words.
     */
    public List<String> getMostLikelyWords(List<String> words, byte numberWordsInterested, String tableName, boolean fileSeparated)
    {
        return getMostLikelyWords(words, numberWordsInterested, null, tableName, fileSeparated);
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability, according to the list of previous words and part of the current word.
     *
     * @param words                 the list of words previous to the ones we want and the current subword.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param files                 the files we are interested in.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the most probable word in a list, according to the list of previous words.
     */
    public List<String> getMostLikelyWords(List<String> words, byte numberWordsInterested, List<Integer> files, String tableName, boolean fileSeparated)
    {
        if (!verifyParameteres(words, numberWordsInterested, "getMostLikelyWords"))
            return null;

        long startTime = System.currentTimeMillis();

        // The size of the sequence previous to the words of interest
        byte numberPreviousWords = (byte) words.size();

        // The column names
        String columnName = "string" + numberPreviousWords;

        String queryConditions = "";
        String groupConditions = "";

        byte i;

        // Add the previous words to the query conditions
        for (i = 0; i < numberPreviousWords; i++)
        {
            // If it is not the first word
            if (i != 0)
            {
                queryConditions += " AND ";

                groupConditions += ", ";
            }

            // If it is the last word then search for LIKE instead of =
            if (i == numberPreviousWords - 1)
                queryConditions += "string" + (i + 1) + " LIKE '" + escape(words.get(i).toLowerCase()) + "%'";
            else
                queryConditions += "string" + (i + 1) + " = '" + escape(words.get(i).toLowerCase()) + "'";

            groupConditions += "string" + (i + 1);
        }

        // Take care of the files to be used
        if (fileSeparated)
        {
            if (files != null)
            {
                if (!files.isEmpty())
                {
                    queryConditions += " AND (";

                    for (i = 0; i < files.size(); i++)
                    {
                        if (i != 0)
                        {
                            queryConditions += " OR ";
                        }

                        queryConditions += "file_id = " + (files.get(i) + initialFile);
                    }

                    queryConditions += ")";
                }
            }
        }

        // The complete query
        String query = "SELECT " + columnName + " FROM " + tableName + " WHERE " + queryConditions + " GROUP BY " + groupConditions + " ORDER BY SUM(count) DESC LIMIT " + numberWordsInterested + ";";

        if (verbose)
            System.out.println(query);

        try
        {
            // The results of the query
            ResultSet rs = stmtQuery.executeQuery(query);

            // The final result
            byte numberWordsObtained = 0;
            List<String> result = new ArrayList<>();

            // If there are still results available and
            // we still need more words
            while (rs.next() && numberWordsObtained < numberWordsInterested)
            {
                result.add(rs.getString("string" + numberPreviousWords));
                numberWordsObtained++;
            }

            // If there are no results
            if (numberWordsObtained == 0)
                return null;

            if (verbose)
                System.out.println("Query getMostLikelyWords took: " + (System.currentTimeMillis() - startTime) + "ms");

            return result;
        }
        catch (SQLException e)
        {
            if (verbose)
            {
                System.err.println("ERROR at getMostLikelyWords: " + e);
                System.err.println(query);
            }

            return null;
        }
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability, according to the list of previous words.
     *
     * @param previousWords         the list of words previous to the ones we want.
     * @param wordsOfInterest       the list of words to choose from.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the most probable word in a list, according to the list of previous words.
     */
    public List<String> getMostLikelyNextWords(List<String> previousWords, List<String> wordsOfInterest, byte numberWordsInterested, String tableName, boolean fileSeparated)
    {
        return getMostLikelyNextWords(previousWords, wordsOfInterest, numberWordsInterested, null, tableName, fileSeparated);
    }

    /**
     * Returns the most probable 'numberWordsInterest' words from a list, according to the list of previous words.
     *
     * @param previousWords         the list of words previous to the ones we want.
     * @param wordsOfInterest       the list of words to choose from.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param files                 the files we are interested in.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the most probable words in a list, according to the list of previous words.
     */
    public List<String> getMostLikelyNextWords(List<String> previousWords, List<String> wordsOfInterest, byte numberWordsInterested, List<Integer> files,
                                               String tableName, boolean fileSeparated)
    {
        if (wordsOfInterest == null)
            return null;

        if (wordsOfInterest.isEmpty())
            return null;

        return getMostLikelyWordsOfInterest(previousWords, null, wordsOfInterest, numberWordsInterested, files, tableName, fileSeparated);
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability of coming within the context of both previous and next words.
     *
     * @param previousWords         the list of words previous to the ones we want.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the list of most likely words.
     */
    public List<String> getMostLikelyWordsOfInterest(List<String> previousWords, byte numberWordsInterested, String tableName, boolean fileSeparated)
    {
        return getMostLikelyWordsOfInterest(previousWords, numberWordsInterested, null, tableName, fileSeparated);
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability of coming within the context of both previous and next words.
     *
     * @param previousWords         the list of words previous to the ones we want.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param files                 the files we are interested in.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the list of most likely words.
     */
    public List<String> getMostLikelyWordsOfInterest(List<String> previousWords, byte numberWordsInterested, List<Integer> files, String tableName, boolean fileSeparated)
    {
        return getMostLikelyWordsOfInterest(previousWords, null, null, numberWordsInterested, files, tableName, fileSeparated);
    }

    /**
     * Returns the 'numberWordsInterested' words with the most probability of coming within the context of both previous and next words.
     *
     * @param previousWords         the list of words previous to the ones we want.
     * @param nextWords             the list of words next to the ones we want.
     * @param wordsOfInterest       the list of words to choose from.
     * @param numberWordsInterested the number of most probable words we are interested in retrieving.
     * @param files                 the files we are interested in.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return the list of most likely words.
     */
    public List<String> getMostLikelyWordsOfInterest(List<String> previousWords, List<String> nextWords, List<String> wordsOfInterest, byte numberWordsInterested,
                                                     List<Integer> files, String tableName, boolean fileSeparated)
    {
        if (previousWords == null && nextWords == null)
        {
            System.err.println("ERROR at getMostLikelyWordsOfInterest: Both the previous and the next words are null.");
            return null;
        }

        long startTime = System.currentTimeMillis();

        // The size of the sequence previous to the words of interest
        byte numberPreviousWords = 0;

        if (previousWords != null)
            numberPreviousWords = (byte) previousWords.size();

        // The size of the sequence next to the words of interest
        byte numberNextWords = 0;

        if (nextWords != null)
            numberNextWords = (byte) nextWords.size();

        // The total size of the n-gram
        byte ngramSize = (byte) (numberPreviousWords + numberNextWords + 1);

        // The column names
        String columnName = "string" + (numberPreviousWords + 1);

        String queryConditions = "";
        String groupConditions = "";

        short i;

        // Add the previous words to the query conditions
        for (i = 0; i < numberPreviousWords; i++)
        {
            if (i != 0)
            {
                queryConditions += " AND ";

                groupConditions += ", ";
            }

            queryConditions += "string" + (i + 1) + " = '" + escape(previousWords.get(i).toLowerCase()) + "'";

            groupConditions += "string" + (i + 1);
        }

        i++;

        if (i != 1)
            groupConditions += ", string" + i;
        else
            groupConditions += "string" + i;

        // Add the next words to the query conditions
        for (; i < ngramSize; i++)
        {
            if (i != (numberPreviousWords + 1) || numberPreviousWords != 0)
                queryConditions += " AND ";

            queryConditions += "string" + (i + 1) + " = '" + escape(nextWords.get(i - numberPreviousWords - 1).toLowerCase()) + "'";

            groupConditions += ", string" + (i + 1);
        }

        // If the list of words of interest is not null
        if (wordsOfInterest != null)
        {
            if (wordsOfInterest.size() == 1)
            {
                List<String> mlw = new ArrayList<>();
                mlw.add(wordsOfInterest.get(0));

                return mlw;
            }

            if (!wordsOfInterest.isEmpty())
            {
                queryConditions += " AND (";

                // Add the words of interest to the query conditions
                for (i = 0; i < wordsOfInterest.size(); i++)
                {
                    if (i != 0)
                        queryConditions += " OR ";

                    queryConditions += "string" + (numberPreviousWords + 1) + " = '" + escape(wordsOfInterest.get(i).toLowerCase()) + "'";
                }

                queryConditions += ")";
            }
        }

        // Take care of the files to be used
        if (fileSeparated)
        {
            if (files != null)
            {
                if (!files.isEmpty())
                {
                    queryConditions += " AND (";

                    for (i = 0; i < files.size(); i++)
                    {
                        if (i != 0)
                        {
                            queryConditions += " OR ";
                        }

                        queryConditions += "file_id = " + (files.get(i));
                    }

                    queryConditions += ")";
                }
            }
        }

        // The complete query
        String query = "SELECT " + columnName + " FROM " + tableName + " WHERE " + queryConditions + " GROUP BY " + groupConditions + " ORDER BY SUM(count) DESC LIMIT " + numberWordsInterested + ";";


        // Get the value from the HashMap
        if (contextResults.containsKey(query))
            return (contextResults.get(query));
        else
        {
            if (verbose)
                System.out.println(query);

            try
            {
                // The results of the query
                ResultSet rs = stmtQuery.executeQuery(query);

                // The final result
                byte numberWordsObtained = 0;
                List<String> result = new ArrayList<>();

                // If there are still results available and
                // we still need more words
                while (rs.next() && numberWordsObtained < numberWordsInterested)
                {
                    result.add(rs.getString(1));
                    numberWordsObtained++;
                }

                if (verbose)
                    System.out.println("Query getMostLikelyWordsOfInterest took: " + (System.currentTimeMillis() - startTime) + "ms");

                // If there are no results
                if (numberWordsObtained == 0)
                    return null;

                contextResults.put(query, result);

                return result;
            }
            catch (SQLException e)
            {
                if (verbose)
                {
                    System.err.println("ERROR at getMostLikelyWordsOfInterest: " + e);
                    System.err.println(query);
                }

                return null;
            }
        }
    }

    /**
     * Get the minor n's that are used to calculate the discount.
     *
     * @param n the order of the n-gram.
     * @param x the count we want the n from.
     *
     * @return the minor n's that are used to calculate the discount.
     */
    public int getLowerNX(int n, int x)
    {
        if (x == 0 || n < 2)
            return 0;

        n -= 2;
        x--;

        int[] n0x = {1524788, 369158, 164947, 96871};   // 2-grams - csTotal2
        int[] n1x = {5865501, 921246, 331098, 171866};  // 3-grams - csTotal

        int[][] nx = new int[2][4];

        nx[0] = n0x;
        nx[1] = n1x;

        return nx[n][x];
    }

    /**
     * Calculate the values of discount according to the n and count.
     *
     * @param n the order of the n-gram.
     * @param c the count we want.
     *
     * @return the discount according to the n and count.
     */
    public float calculateD(int n, int c)
    {
        //System.out.println("n: " + n + " c: " + c);

        int n1, n2, n3, n4;
        float y;

        if(c > 3)
            c = 3;

        switch (c)
        {
            case 0:
                return 0;

            case 1:
                n1 = getLowerNX(n, 1);
                n2 = getLowerNX(n, 2);

                y = (float) n1 / (n1 + 2 * n2);

                //System.out.println(y);
                //System.out.println("n1: " + n1 + " n2: " + n2);
                //System.out.println(1 - 2 * y * (n2 / (float)n1));

                return 1 - 2 * y * (n2 / (float) n1);

            case 2:
                n1 = getLowerNX(n, 1);
                n2 = getLowerNX(n, 2);
                n3 = getLowerNX(n, 3);

                y = (float) n1 / (n1 + 2 * n2);

                //System.out.println(y);
                //System.out.println("n1: " + n1 + " n2: " + n2 + " n3: " + n3);
                //System.out.println(2 - 3 * y * (n3 / (float)n2));

                return 2 - 3 * y * (n3 / (float) n2);

            case 3:
                n1 = getLowerNX(n, 1);
                n2 = getLowerNX(n, 2);
                n3 = getLowerNX(n, 3);
                n4 = getLowerNX(n, 4);

                y = (float) n1 / (n1 + 2 * n2);

                //System.out.println(y);
                //System.out.println("n1: " + n1 + " n2: " + n2 + " n3: " + n3 + " n4: " + n4);
                //System.out.println(3 - 4 * y * (n4 / (float)n3));

                return 3 - 4 * y * (n4 / (float) n3);
        }

        System.err.println("ERROR in calculateD: case was not between 0 and 3: " + c);
        return 0;
    }

    /**
     * Get the total number of n-grams with a predefined history.
     *
     * @param history       the previous words from the n-gram.
     * @param start         the position offset for the words in the history.
     * @param n             the order of the n-gram. Used to select the table.
     * @param column        the column used in the search.
     * @param specificCount true if it should select n-grams with a specific count.
     * @param c             the specific count if the 'specificCount' is true.
     * @param files         the specific files for the n-grams.
     * @param tableName     the name of the table for the 3-grams.
     * @param fileSeparated if the files should be identified separately.
     *
     * @return the total number of n-grams with a predefined history.
     */
    public int getTotalNgrams(List<String> history, int start, int n, String column, boolean specificCount, int c, List<Integer> files, String tableName, boolean fileSeparated)
    {
        if (n == 2)
            tableName += 2;

        // The query conditions
        String queryConditions = "";

        short i;

        // Add all the history
        if (history != null)
            for (i = 0; i < history.size(); i++)
            {
                if (i != 0)
                    queryConditions += " AND ";

                queryConditions += "string" + (i + 1 + start) + " = '" + escape(history.get(i).toLowerCase()) + "'";
            }

        // Take care of the files to be used
        if (fileSeparated)
        {
            if (files != null)
            {
                if (!files.isEmpty())
                {
                    if (!queryConditions.equals(""))
                        queryConditions += " AND ";

                    queryConditions += "(";

                    for (i = 0; i < files.size(); i++)
                    {
                        if (i != 0)
                        {
                            queryConditions += " OR ";

                        }

                        queryConditions += "file_id = " + (files.get(i) + initialFile);
                    }

                    queryConditions += ")";
                }
            }
        }

        String query = "SELECT " + column + " FROM " + tableName;

        if (!queryConditions.equals(""))
            query += " WHERE " + queryConditions;

        if (specificCount)
        {
            if (!queryConditions.equals(""))
                query += " AND ";

            query += "COUNT =" + c;
        }

        query += ";";

        if (verbose)
            System.out.println(query);

        try
        {
            // The result of the query
            ResultSet rs = stmtQuery.executeQuery(query);

            // Get the number of occurrences of the previous sequence
            if (rs.next())
                return (rs.getInt(column));
            else
                return 0;
        }
        catch (SQLException e)
        {
            if (verbose)
            {
                System.err.println("ERROR at getTotalNgrams in query 1: " + e);
                System.err.println(query);
            }

            return -1;
        }
    }

    /**
     * Calculate the gamma for the history of an n-gram.
     *
     * @param history       the previous words from the n-gram.
     * @param files         the files we are interested in.
     * @param tableName     the name of the table.
     * @param fileSeparated true if the table separates the n-grams into files.
     *
     * @return the gamma for the history of an n-gram.
     */
    public float calculateGamma(List<String> history, List<Integer> files, String tableName, boolean fileSeparated)
    {
        int n = history.size() + 1;

        float D1 = calculateD(n, 1);
        float D2 = calculateD(n, 2);
        float D3 = calculateD(n, 3);

        // Get the total number of words that follow the history
        int ci = getTotalNgrams(history, 0, n, "SUM(COUNT)", false, 0, files, tableName, fileSeparated);

        if (ci == 0)
            return 0;

        // Get the number of words that follow the history with count equal to 1l
        int N1 = getTotalNgrams(history, 0, n, "COUNT(*)", true, 1, files, tableName, fileSeparated);

        // Get the number of words that follow the history with count equal to 2
        int N2 = getTotalNgrams(history, 0, n, "COUNT(*)", true, 2, files, tableName, fileSeparated);

        // Get the number of words that follow the history
        int N3 = getTotalNgrams(history, 0, n, "COUNT(*)", true, 3, files, tableName, fileSeparated) + getTotalNgrams(history, 0, n, "COUNT(*)", true, 4, files, tableName, fileSeparated);

        //System.out.println("n: " + n + " -> " + D1 + "*" + N1 + " + " + D2 + "*" + N2 + " + " + D3 + "*" + N3 + "/" + ci);

        return (D1 * N1 + D2 * N2 + D3 * N3) / ci;
    }

    /**
     * Probability of an unigram using Kneser-Ney Smoothing.
     *
     * @param wordOfInterest the word that forms the n-gram with the history.
     * @param files          the files we are interested in.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the probability of the unigram.
     */
    public float probabilityKNUni(String wordOfInterest, List<Integer> files, String tableName, boolean fileSeparated)
    {
        List<String> previous = new ArrayList<>();
        previous.add(wordOfInterest);

        // The number of words that appear in the corpus before the word
        int N1 = getTotalNgrams(previous, 1, 2, "COUNT(*)", false, 0, files, tableName, fileSeparated);

        // The total number of bigrams
        //int bi = getTotalNgrams(null,     0, 2, false, 0, files, tableName, fileSeparated);
        int bi = 2549469;

        return N1 / bi;
    }

    /**
     * Probability of an n-gram using Kneser-Ney Smoothing.
     *
     * @param previousWords  the words that form the history of the n-gram.
     * @param wordOfInterest the word that forms the n-gram with the history.
     * @param files          the files we are interested in.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the probability of the n-gram.
     */
    public float probabilityKN(List<String> previousWords, String wordOfInterest, List<Integer> files, String tableName, boolean fileSeparated)
    {
        // Get the result for the unigram
        if (previousWords == null || previousWords.isEmpty())
            return probabilityKNUni(wordOfInterest, files, tableName, fileSeparated);

        float gamma = calculateGamma(previousWords, files, tableName, fileSeparated);

        List<String> ngram = new ArrayList<>();
        ngram.addAll(previousWords);
        ngram.add(wordOfInterest);

        // The count of the n-gram in the corpus
        int count = getTotalNgrams(ngram, 0, ngram.size(), "count", false, 0, files, tableName, fileSeparated);

        // The discount according to the count
        float d = calculateD(ngram.size(), count);

        // The number of words that follow the history of this n-gram
        int ci = getTotalNgrams(previousWords, 0, ngram.size(), "SUM(COUNT)", false, 0, files, tableName, fileSeparated);

        // Prepare for calculating the probability of the (n-1)-gram
        previousWords.remove(0);

        float res = 0;

        if (ci != 0)
            res = ((count - d) / ci);

        float n1gram = probabilityKN(previousWords, wordOfInterest, files, tableName, fileSeparated);

        return res + gamma * n1gram;
    }

    /**
     * Returns the probability of this n-gram.
     * The size of the previous words plus the word of interest must not exceed the maximum size for a n-gram.
     *
     * @param previousWords  the words that came before the wordsOfInterest, used to estimate the probability.
     * @param wordOfInterest the word we want to test for probability after the sequence.
     * @param nextWords      the words that come after the wordsOfInterest, used to estimate the probability.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the probability of this n-gram.
     */
    public double getProbabilityNgram(List<String> previousWords, String wordOfInterest, List<String> nextWords, String tableName, boolean fileSeparated)
    {
        return getProbabilityNgram(previousWords, wordOfInterest, nextWords, null, tableName, fileSeparated);
    }

    /**
     * Returns the probability of this n-gram.
     * The size of the previous words plus the word of interest and next words must not exceed the maximum size for a n-gram.
     *
     * @param previousWords  the words that came before the wordsOfInterest, used to estimate the probability.
     * @param wordOfInterest the word we want to test for probability after the sequence.
     * @param nextWords      the words that come after the wordsOfInterest, used to estimate the probability.
     * @param files          the files we are interested in.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the probability of this n-gram.
     */
    public double getProbabilityNgram(List<String> previousWords, String wordOfInterest, List<String> nextWords, List<Integer> files, String tableName, boolean fileSeparated)
    {
        // If it is supposed to use the probability with Kneser-Ney Smoothing
        if (probabilityKN)
        {
            List<String> ngramPrevious = new ArrayList<>();
            ngramPrevious.addAll(previousWords);

            // Copy the list
            // Used because of the removes
            List<String> ngramNext = new ArrayList<>();

            if (nextWords != null)
                ngramNext.addAll(nextWords);

            String wOI = new String(wordOfInterest);


            // Treat the case when the words come in the next
            if (ngramNext != null && !ngramNext.isEmpty())
            {
                ngramPrevious.add(wOI);

                for (int i = 0; i < ngramNext.size() - 1; i++)
                    ngramPrevious.add(ngramNext.remove(i));

                wOI = ngramNext.remove(0);
            }

            return probabilityKN(ngramPrevious, wOI, files, tableName, fileSeparated);
        }
        else
        {
            if (wordOfInterest == null)
            {
                System.err.println("ERROR at getProbabilityNgram: Word of interest is null.");
                return -1;
            }

            if (previousWords == null && nextWords == null)
            {
                System.err.println("ERROR at getProbabilityNgram: Both previous words and next words are null.");
                return -1;
            }

            byte numbWords = 0;

            if (previousWords != null)
                numbWords += previousWords.size();

            if (nextWords != null)
                numbWords += nextWords.size();

            if (numbWords + 1 > 3)
            {
                System.err.println("ERROR at getProbabilityNgram: The size of the n-gram is too big.");
                return -1;
            }

            long startTime = System.currentTimeMillis();

            // The query conditions
            String queryConditions = "";

            short i, prevSize;

            // If previous words is null
            if (previousWords == null)
                prevSize = 0;
            else
            {
                prevSize = (byte) previousWords.size();

                // Add all the previous words ot the query conditions
                for (i = 0; i < prevSize; i++)
                {
                    if (i != 0)
                        queryConditions += " AND ";

                    queryConditions += "string" + (i + 1) + " = '" + escape(previousWords.get(i).toLowerCase()) + "'";
                }
            }

            if (nextWords != null)
                // Add all the next words ot the query conditions
                for (i = (byte) (prevSize + 1); i < nextWords.size() + prevSize + 1; i++)
                {
                    if (prevSize > 0 || i != (prevSize + 1))
                        queryConditions += " AND ";

                    queryConditions += "string" + (i + 1) + " = '" + escape(nextWords.get(i - (prevSize + 1)).toLowerCase()) + "'";
                }

            // Take care of the files to be used
            if (fileSeparated)
            {
                if (files != null)
                {
                    if (!files.isEmpty())
                    {
                        queryConditions += " AND (";

                        for (i = 0; i < files.size(); i++)
                        {
                            if (i != 0)
                            {
                                queryConditions += " OR ";

                            }

                            queryConditions += "file_id = " + (files.get(i) + initialFile);
                        }

                        queryConditions += ")";
                    }
                }
            }

            // The number of times the sequence occurs
            int numberSequenceOccurrences;

            String query = "SELECT SUM(count) FROM " + tableName + " WHERE " + queryConditions + ";";

            if (verbose)
                System.out.println(query);

            // Get the value from the HashMap
            if (sumCounts.containsKey(query))
                numberSequenceOccurrences = sumCounts.get(query);
            else
            {
                try
                {
                    // The result of the query
                    ResultSet rs = stmtQuery.executeQuery(query);

                    // Get the number of occurrences of the previous sequence
                    if (rs.next())
                    {
                        numberSequenceOccurrences = rs.getInt("SUM(count)");

                        // Update the HashMap
                        sumCounts.put(query, numberSequenceOccurrences);
                    }
                    else
                    {
                        if (verbose)
                        {
                            System.err.println("ERROR at getProbabilityNgram in query 1: No result from database.");
                            System.err.println(query);
                        }

                        return -1;
                    }
                }
                catch (SQLException e)
                {
                    if (verbose)
                    {
                        System.err.println("ERROR at getProbabilityNgram in query 1: " + e);
                        System.err.println(query);
                    }

                    return -1;
                }
            }

            if (numberSequenceOccurrences == 0)
            {
                if (verbose)
                    System.out.println("Probability: 0.00000000001");

                return 0.00000000001;
            }

            if (verbose)
                System.out.println("Query 1 for getProbabilityNgram took: " + (System.currentTimeMillis() - startTime) + "ms");

            startTime = System.currentTimeMillis();

            // Add the word of interest
            queryConditions += " AND string" + (prevSize + 1) + " = '" + escape(wordOfInterest.toLowerCase()) + "'";

            // The complete query
            query = "SELECT SUM(count) FROM " + tableName + " WHERE " + queryConditions + ";";

            if (verbose)
                System.out.println(query);

            try
            {
                // The result of the query
                ResultSet rs = stmtQuery.executeQuery(query);

                // Get the number of occurrences of the complete expression
                if (rs.next())
                {
                    int count = rs.getInt("SUM(count)");

                    if (verbose)
                        if (count == 0)
                            System.out.println("Probability: 0.00000000001");
                        else
                            System.out.println("Probability: " + (count / (float) numberSequenceOccurrences));

                    if (verbose)
                        System.out.println("Query 2 for getProbabilityNgram took: " + (System.currentTimeMillis() - startTime) + "ms");

                    if (count == 0)
                        return 0.00000000001;

                    // The resulting probability
                    return count / (float) numberSequenceOccurrences;
                }
                else
                {
                    if (verbose)
                    {
                        System.err.println("ERROR at getProbabilityNgram in query 2: No result from database.");
                        System.err.println(query);
                    }

                    return -1;
                }
            }
            catch (SQLException e)
            {
                if (verbose)
                {
                    System.err.println("ERROR at getProbabilityNgram in query 2: " + e);
                    System.err.println(query);
                }

                return -1;
            }
        }
    }

    /**
     * Returns the probability of the expression formed by the previous words, word of interest and next words.
     * This allows for an expression bigger than the size of a n-gram.
     * This method updates the previous words and word of interest.
     *
     * @param previousWords  the words that came before the wordsOfInterest, used to estimate the probability.
     * @param wordOfInterest the word we want to test for probability after the sequence.
     * @param nextWords      the words that come after the wordsOfInterest, used to estimate the probability.
     * @param files          the files we are interested in.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the proability of the expression formed by the previous words, word of interest and next words.
     */
    public double getProbabilityExpression(List<String> previousWords, String wordOfInterest, List<String> nextWords, List<Integer> files,
                                           String tableName, boolean fileSeparated)
    {
        if (previousWords.size() > 4 || previousWords.isEmpty())
        {
            System.err.println("ERROR at getProbabilityExpression: Not prepared for lists of previous words with size different from the size of ngram - 1.");
            return -1;
        }

        if (wordOfInterest == null)
        {
            System.err.println("ERROR at getProbabilityExpression: Word of interest is null.");
            return -1;
        }

        // Copy the list
        // Used because of the removes
        List<String> ngramPrevious = new ArrayList<>();
        ngramPrevious.addAll(previousWords);

        // Copy the list
        // Used because of the removes
        List<String> ngramNext = new ArrayList<>();
        ngramNext.addAll(nextWords);

        double probability = 0, p;

        // Works with all the previous words, word of interest and next words to form an expression and calculate its probability
        // by splitting into ngrams and adding the log of the probability of each
        while (!ngramNext.isEmpty())
        {
            p = getProbabilityNgram(ngramPrevious, wordOfInterest, null, files, tableName, fileSeparated);

            if (p == -1)
                return Double.POSITIVE_INFINITY;

            probability += Math.log(p);

            // Update the variables for the next ngram
            ngramPrevious.remove(0);
            ngramPrevious.add(wordOfInterest);
            wordOfInterest = ngramNext.remove(0);
        }

        p = getProbabilityNgram(ngramPrevious, wordOfInterest, null, files, tableName, fileSeparated);

        if (p == -1)
            return Double.POSITIVE_INFINITY;

        probability += Math.log(p);

        return probability;
    }

    /**
     * Returns the probability of the expression formed by the previous words, word of interest and next words.
     * This allows for an expression bigger than the size of a n-gram.
     * This method updates the previous words and next words, only changing the position of the word of interest.
     *
     * @param previousWords  the words that came before the wordsOfInterest, used to estimate the probability.
     * @param wordOfInterest the word we want to test for probability after the sequence.
     * @param nextWords      the words that come after the wordsOfInterest, used to estimate the probability.
     * @param files          the files we are interested in.
     * @param tableName      the name of the table.
     * @param fileSeparated  true if the table separates the n-grams into files.
     *
     * @return the proability of the expression formed by the previous words, word of interest and next words.
     */
    public double getProbabilityExpression2(List<String> previousWords, String wordOfInterest, List<String> nextWords, List<Integer> files,
                                            String tableName, boolean fileSeparated)
    {
        if (previousWords.size() > 4 || previousWords.isEmpty())
        {
            System.err.println("ERROR at getProbabilityExpression2: Not prepared for lists of previous words with size different from the size of ngram - 1.");
            return Double.NEGATIVE_INFINITY;
        }

        if (wordOfInterest == null)
        {
            System.err.println("ERROR at getProbabilityExpression2: Word of interest is null.");
            return Double.NEGATIVE_INFINITY;
        }

        // Copy the list
        // Used because of the removes
        List<String> ngramPrevious = new ArrayList<>();
        ngramPrevious.addAll(previousWords);

        // Copy the list
        // Used because of the removes
        List<String> ngramNext = new ArrayList<>();
        ngramNext.addAll(nextWords);

        List<String> next = new ArrayList<>();

        double probability = 0, p;

        // Works with all the previous words, word of interest and next words to form an expression and calculate its probability
        // by splitting into n-grams and adding the log of the probability of each
        while (!ngramNext.isEmpty())
        {
            p = getProbabilityNgram(ngramPrevious, wordOfInterest, next, files, tableName, fileSeparated);

            if (p != -1)
                probability += Math.log(p);
            else
                probability += Double.NEGATIVE_INFINITY;

            // Update the variables for the next n-gram
            ngramPrevious.remove(0);
            next.add(ngramNext.remove(0));
        }

        p = getProbabilityNgram(ngramPrevious, wordOfInterest, next, files, tableName, fileSeparated);

        if (p != -1)
            probability += Math.log(p);
        else
            probability += Double.NEGATIVE_INFINITY;

        if (probability == 0)
            return -999999999999d;

        return probability;
    }

    /**
     * Returns a list with the numberWordsInterested most probable words according to the sequence that came previously, the words of interest and the words that come after.
     *
     * @param previousWords         the words that came before the wordsOfInterest, used to estimate the probability.
     * @param wordsOfInterest       the words we want to test for probability after the sequence.
     * @param nextWords             the words that come after the wordsOfInterest, used to estimate the probability.
     * @param numberWordsInterested the number of most probable words we interested in retrieving.
     * @param files                 the files we are interested in.
     * @param tableName             the name of the table.
     * @param fileSeparated         true if the table separates the n-grams into files.
     *
     * @return a list with the numberWordsInterested most probable words according to the sequence that came previously, the words of interest and the words that come after.
     */
    public List<SuggestedReplacement> getMostLikelyExpressions(List<String> previousWords, List<SuggestedReplacement> wordsOfInterest, List<String> nextWords, int numberWordsInterested,
                                                               List<Integer> files, String tableName, boolean fileSeparated)
    {
        if (previousWords.size() > 4 || previousWords.isEmpty())
        {
            System.err.println("ERROR at getMostLikelyExpressions: Not prepared for lists of previous words with size different from the size of ngram - 1.");
            return null;
        }

        if (wordsOfInterest.isEmpty())
        {
            System.err.println("ERROR at getMostLikelyExpressions: List of words of interest empty.");
            return null;
        }

        // These are used to calculate the 'numberWordsInterested' most probable expressions
        List<Double> wordProbability = new ArrayList<>();
        List<Integer> wordPosition = new ArrayList<>();

        int i, j;

        double p;

        // Erase all previous counts
        sumCounts.clear();

        // Get the probability of each expression and discover the most probable expressions
        for (i = 0; i < wordsOfInterest.size(); i++)
        {
            if (verbose)
                System.out.println("\nAnalyse Expression: " + previousWords + " + " + wordsOfInterest.get(i).getReplacement() + " + " + nextWords);

            //p = getProbabilityExpression(previousWords, wordsOfInterest.get(i), nextWords);
            p = getProbabilityExpression2(previousWords, wordsOfInterest.get(i).getReplacement(), nextWords, files, tableName, fileSeparated);

            if (verbose)
                System.out.println("Probability Expression: " + p);

            if (p == Double.NEGATIVE_INFINITY)
                continue;

            // If the list is empty - add the new probability
            if (wordProbability.isEmpty())
            {
                wordProbability.add(p);
                wordPosition.add(i);
            }
            // If this has bigger probability than the one with the least probability - place it at the correct position
            else if (p > wordProbability.get(wordProbability.size() - 1))
            {
                for (j = wordProbability.size() - 1; j >= 0; j--)
                    if (p < wordProbability.get(j))
                        break;

                wordProbability.add(j + 1, p);
                wordPosition.add(j + 1, i);

                // If the size of the list is bigger than what we want, remove the one with the least probability
                if (wordProbability.size() > numberWordsInterested)
                {
                    wordProbability.remove(wordProbability.size() - 1);
                    wordPosition.remove(wordPosition.size() - 1);
                }
                // If there's space left - place it at the end
            }
            else if (wordProbability.size() < numberWordsInterested)
            {
                wordProbability.add(p);
                wordPosition.add(i);
            }
        }

        List<SuggestedReplacement> result = new ArrayList<>();

        if (wordProbability.size() > numberWordsInterested)
        {
            System.err.println("ERROR at getMostLikelyExpressions: List of words is bigger than we wanted.");
            return null;
        }

        for (i = 0; i < wordProbability.size(); i++)
            result.add(wordsOfInterest.get(wordPosition.get(i)));

        return result;
    }

    /**
     * Calculate and insert n-grams for all the text files in a directory to the database.
     *
     * @param directoryFile       the directory.
     * @param tableName           the name of the table.
     * @param filesTableName      the name of the table that contains the references to each file.
     * @param insertFileReference True if the file reference should be inserted in the filesTableName.
     * @param topicName           the name of the topic.
     * @param n                   the order of the n-gram.
     */
    public void fillTableFromDirectory(File directoryFile, String tableName, String filesTableName, String topicName, int n, boolean insertFileReference)
    {
        boolean noPunctuation = true;
        boolean noSpecificNumbers = true;

        NGram ngram;
        float time;

        if (directoryFile.isDirectory())
        {
            if (directoryFile.getName().equals("Test"))
                return;

            if (directoryFile.getName().equals("Validation"))
                return;

            time = System.currentTimeMillis();

            File[] listOfFiles = directoryFile.listFiles();

            if (listOfFiles != null)
                for (File singleFile : listOfFiles)
                    fillTableFromDirectory(singleFile, tableName, filesTableName, topicName, n, insertFileReference);

            System.out.println(directoryFile.getName() + " took " + (System.currentTimeMillis() - time) + " ms.");
        }
        else
        {
            ngram = new NGram(n);

            if (insertFileReference)
                insertFileReference(directoryFile, filesTableName, tableName);

            ngram.calculateNgramCountsFile(directoryFile, noPunctuation, noSpecificNumbers);

            insertNGramsFromFile(ngram, tableName, directoryFile, filesTableName, topicName, n);
        }
    }

    public void insertFileReference(File directoryFile, String filesTableName, String tableName)
    {
        String query = "INSERT INTO " + filesTableName + " (name, topic) VALUES (\"" + directoryFile.getName() + "\", \"" + tableName + "\")";

        try
        {
            stmtQuery.executeUpdate(query);
        }
        catch (SQLException e)
        {
            if (verbose)
            {
                System.err.println("ERROR at insertNGramsFromFile - Inserting into file3gram:" + e);
                System.err.println(query);
            }
        }
    }

    /**
     * Calculate and insert the n-grams of a text file to the database, specifying the file_id.
     *
     * @param ngram          the n-gram counts.
     * @param tableName      the name of the table.
     * @param textFile       the text file to be added.
     * @param filesTableName the name of the table that contains the references to each file.
     * @param topicName      the name of the topic.
     * @param n              the order of the n-gram.
     */
    public void insertNGramsFromFile(NGram ngram, String tableName, File textFile, String filesTableName, String topicName, int n)
    {
        String query = "SELECT (id) FROM " + filesTableName + " WHERE name=\"" + textFile.getName() + "\" AND topic=\"" + topicName + "\"";

        ResultSet rs;
        int file_id;

        try
        {
            rs = stmtQuery.executeQuery(query);

            // Get the number of occurrences of the complete expression
            if (rs.next())
            {
                file_id = rs.getInt("id");
            }
            else
                return;
        }
        catch (SQLException e)
        {
            if (verbose)
            {
                System.err.println("ERROR at insertNGramsFromFile - Selecting the id:" + e);
                System.err.println(query);
            }

            return;
        }

        query = "INSERT INTO " + tableName + " (count,";

        String value = "";

        for (int i = 0; i < n; i++)
        {
            query += " string" + (i + 1) + ",";
            value += ", ?";
        }

        query += " file_id) VALUES (?, ?";
        query += value + ")";

        Iterator it = ngram.getNgramCounts().entrySet().iterator();

        List<String> ngramC;
        Map.Entry pair;

        try
        {
            PreparedStatement p = dbConnection.prepareStatement(query);
            dbConnection.setAutoCommit(false);

            while (it.hasNext())
            {
                pair = (Map.Entry) it.next();

                it.remove(); // avoids a ConcurrentModificationException

                ngramC = (List<String>) pair.getKey();

                if (ngramC.size() != n)
                {
                    continue;
                }

                // Set the count
                p.setInt(1, (Integer) pair.getValue());

                // Set each of the strings
                for (int i = 0; i < n; i++)
                    p.setString(i + 2, ngramC.get(i));

                // Set the file_id
                p.setInt(n + 2, file_id);

                p.addBatch();
            }

            p.executeBatch();
            dbConnection.commit();
        }
        catch (SQLException e)
        {
            if (verbose)
            {
                System.err.println("ERROR at insertNGramsFromFile - Inserting into file3gram:" + e);
                System.err.println(query);
            }
        }
    }

    /**
     * Queries the database for the first file in the table for that topic.
     * This is necessary because when you're comparing with the topics distribution it will always start in 0.
     *
     * @param filesTableName the name of the table that saves the information about the files-
     * @param topicName      the name of the topic of interest.
     */
    public void getInitialFile(String filesTableName, String topicName)
    {
        String query = "SELECT id FROM " + filesTableName;

        if (topicName != null)
            query += " WHERE topic=\"" + topicName;

        query += "\" ORDER BY id LIMIT 1";

        ResultSet rs;

        try
        {
            rs = stmtQuery.executeQuery(query);

            // Get the number of occurrences of the complete expression
            if (rs.next())
            {
                initialFile = rs.getInt("id");
            }
            else
            {
                System.err.println("ERROR at getInitialFile: No file was found for that topic.");
                System.exit(1);
            }
        }
        catch (SQLException e)
        {
            System.err.println("ERROR at getInitialFile - No file was found for that topic:");
            System.err.println(query);
            e.printStackTrace();

            System.exit(1);
        }
    }


    /**
     * Code to escape characters for MySQL.
     * Done by Walid, can be found at:
     * http://stackoverflow.com/questions/881194/how-do-i-escape-special-characters-in-mysql
     */
    private static final HashMap<String, String> sqlTokens;
    private static Pattern sqlTokenPattern;

    static
    {
        //MySQL escape sequences: http://dev.mysql.com/doc/refman/5.1/en/string-syntax.html
        String[][] search_regex_replacement = new String[][]
                {
                        //search string     search regex        sql replacement regex
                        {"\u0000", "\\x00", "\\\\0"},
                        {"'", "'", "\\\\'"},
                        {"\"", "\"", "\\\\\""},
                        {"\b", "\\x08", "\\\\b"},
                        {"\n", "\\n", "\\\\n"},
                        {"\r", "\\r", "\\\\r"},
                        {"\t", "\\t", "\\\\t"},
                        {"\u001A", "\\x1A", "\\\\Z"},
                        {"\\", "\\\\", "\\\\\\\\"}
                };

        sqlTokens = new HashMap<String, String>();
        String patternStr = "";
        for (String[] srr : search_regex_replacement)
        {
            sqlTokens.put(srr[0], srr[2]);
            patternStr += (patternStr.isEmpty() ? "" : "|") + srr[1];
        }
        sqlTokenPattern = Pattern.compile('(' + patternStr + ')');
    }

    public static String escape(String s)
    {
        Matcher matcher = sqlTokenPattern.matcher(s);
        StringBuffer sb = new StringBuffer();
        while (matcher.find())
        {
            matcher.appendReplacement(sb, sqlTokens.get(matcher.group(1)));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}