package auxiliary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acácio Correia on 08/07/2016.
 */
public class Sentence
{
    private String sentence;
    private int startPos;
    private int endPos;
    private boolean wasEdited;

    private List<Suggestion> suggestions;

    public Sentence(String sentence, int startPos, int endPos, boolean wasEdited)
    {
        this.sentence = sentence;
        this.startPos = startPos;
        this.endPos = endPos;
        this.wasEdited = wasEdited;

        suggestions = new ArrayList<>();
    }

    public Sentence(String sentence, int startPos, int endPos)
    {
        this(sentence, startPos, endPos, true);
    }

    public String getSentence()
    {
        return sentence;
    }

    public int getStartPos()
    {
        return startPos;
    }

    public int getEndPos()
    {
        return endPos;
    }

    public boolean wasEdited()
    {
        return wasEdited;
    }

    public List<Suggestion> getSuggestions()
    {
        return suggestions;
    }

    public void setSentence(String sentence)
    {
        this.sentence = sentence;
    }

    public void setStartPos(int startPos)
    {
        this.startPos = startPos;
    }

    public void setEndPos(int endPos)
    {
        this.endPos = endPos;
    }

    public void setWasEdited(boolean wasEdited)
    {
        this.wasEdited = wasEdited;
    }

    @Override
    public String toString()
    {
        return "Sentence{" +
                "sentence='" + sentence + '\'' +
                ", startPos=" + startPos +
                ", endPos=" + endPos +
                ", wasEdited=" + wasEdited +
                '}';
    }
}