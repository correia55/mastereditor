package auxiliary;

import simplenlg.features.Feature;
import simplenlg.features.NumberAgreement;
import simplenlg.features.Person;
import simplenlg.features.Tense;
import simplenlg.framework.InflectedWordElement;
import simplenlg.framework.LexicalCategory;
import simplenlg.framework.WordElement;
import simplenlg.lexicon.Lexicon;
import simplenlg.realiser.english.Realiser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acácio Correia on 18/05/2016.
 */
public class SimpleNLG
{
    // The verbosity of the output
    private final boolean verbose;

    // Used to search for words
    private Lexicon lexicon;

    // Used to perform the morphological realisation of the base word
    private Realiser realiser;

    // Word informations
    private String baseWord;
    private String posTag;

    private List<InflectedWordElement> inflectedElements;

    public static void main(String[] args)
    {
        SimpleNLG snlg = new SimpleNLG(true, "be", "VBZ");

        System.out.println(snlg.getVariants());
    }

    public SimpleNLG(boolean verbose)
    {
        this.verbose = verbose;

        lexicon = Lexicon.getDefaultLexicon();
        realiser = new Realiser(lexicon);

        inflectedElements = new ArrayList<>();
    }

    public SimpleNLG(boolean verbose, String word, String posTag)
    {
        this(verbose);

        setWord(word, posTag);
    }

    /**
     * Defines the original word and tag, extracting its elements.
     *
     * @param word the word to analyse.
     * @param posTag the tag of the word.
     */
    public void setWord(String word, String posTag)
    {
        this.posTag = posTag;

        LexicalCategory category = getCategory(posTag);

        baseWord = word;

        if (!lexicon.hasWordFromVariant(word) && verbose)
            System.err.println("ERROR: " + word + " with tag " + posTag + " variant was not found.");

        inflectedElements.clear();

        // Extracts the words from a word variant
        for (WordElement w : lexicon.getWordsFromVariant(word, category))
            inflectedElements.add(new InflectedWordElement(w));
    }

    /**
     * Get the word category.
     *
     * @param posTag the tag of the word.
     * @return the category.
     */
    public LexicalCategory getCategory(String posTag)
    {
        char firstCharTag = posTag.charAt(0);

        if (posTag.length() < 2)
            return null;

        char secondCharTag = posTag.charAt(1);

        // If the tag's first word is a J then the word is an adjective
        if (firstCharTag == 'J')
            return LexicalCategory.ADJECTIVE;

        // If the tag's first word is a N then the word is a noun
        else if (firstCharTag == 'N')
            return LexicalCategory.NOUN;

        // If the tag's first word is a V then the word is a verb
        else if (firstCharTag == 'V')
            return LexicalCategory.VERB;

        // If the tag starts with "RB" then the word is an adverb
        else if (firstCharTag == 'R' && secondCharTag == 'B')
            return LexicalCategory.ADVERB;
        else
            return null;
    }

    /**
     * Determines the list of the resulting variants for the base word by extracting information from the respective tag.
     * @return the list of resulting variants.
     */
    public List<String> getVariants()
    {
        List<String> inflectedWords = new ArrayList<>();

        switch (posTag)
        {
            case "NNS":

            case "NNPS":
                for (InflectedWordElement inflectedElement : inflectedElements)
                {
                    inflectedElement.setFeature(Feature.NUMBER, NumberAgreement.PLURAL);

                    inflectedWords.add(realiser.realise(inflectedElement).toString());
                }

                break;

            case "NNP":
                for (InflectedWordElement inflectedElement : inflectedElements)
                {
                    inflectedElement.setFeature(Feature.NUMBER, NumberAgreement.SINGULAR);

                    inflectedWords.add(realiser.realise(inflectedElement).toString());
                }

                break;

            case "VBD":
                for (InflectedWordElement inflectedElement : inflectedElements)
                {
                    inflectedElement.setFeature(Feature.TENSE, Tense.PAST);

                    inflectedWords.add(realiser.realise(inflectedElement).toString());
                }

                break;

            case "VBZ":
                for (InflectedWordElement inflectedElement : inflectedElements)
                {
                    inflectedElement.setFeature(Feature.NUMBER, NumberAgreement.SINGULAR);
                    inflectedElement.setFeature(Feature.TENSE, Tense.PRESENT);
                    inflectedElement.setFeature(Feature.PERSON, Person.THIRD);

                    inflectedWords.add(realiser.realise(inflectedElement).toString());
                }

                break;
        }

        if(inflectedWords.isEmpty())
            inflectedWords.add(baseWord);

        return inflectedWords;
    }
}