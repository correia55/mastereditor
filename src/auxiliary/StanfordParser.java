package auxiliary;

import edu.mit.jwi.item.POS;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;

import java.io.StringReader;
import java.util.List;

/**
 * Based on the manual.
 *
 * @author Acácio Correia
 */
public class StanfordParser
{
    private boolean verbose;

    private LexicalizedParser lp;
    private TokenizerFactory<CoreLabel> tokenizerFactory;

    public static void main(String[] args)
    {
        StanfordParser sp = new StanfordParser();

        // This option shows loading and using an explicit tokenizer
        String sent = "Dr. Maboul is an experimental sentence.";

        /*
        // This option shows loading and using an explicit tokenizer
        String sent = "This is another sentence.";
        System.out.println(sent);
        
        TokenizerFactory<CoreLabel> tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
        Tokenizer<CoreLabel> tok = tokenizerFactory.getTokenizer(new StringReader(sent));
        List<CoreLabel> rawWords2 = tok.tokenize();
        Tree parse = lp.apply(rawWords2);
        
        System.out.println("parse.pennPrint():");
        parse.pennPrint();
        
        // PennTreebankLanguagePack for English
        TreebankLanguagePack tlp = lp.treebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        List<TypedDependency> tdl = gs.typedDependenciesCCprocessed();
        System.out.println("tdl:");
        System.out.println(tdl);

        // You can also use a TreePrint object to print trees and dependencies
        TreePrint tp = new TreePrint("penn,typedDependenciesCollapsed");
        System.out.println("tp.printTree(parse):");
        tp.printTree(parse);*/
    }

    public StanfordParser()
    {
        this(true);
    }

    public StanfordParser(boolean verbose)
    {
        this("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz", verbose);
    }

    public StanfordParser(String parserModel, boolean verbose)
    {
        lp = LexicalizedParser.loadModel(parserModel);
        this.verbose = verbose;

        tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
    }

    /**
     * Parse the sentence, returning the list of tagged words.
     *
     * @param sentence the sentence to parse.
     *
     * @return the list of tagged words.
     */
    public List<TaggedWord> parseSentence(String sentence)
    {
        Tokenizer<CoreLabel> tok = tokenizerFactory.getTokenizer(new StringReader(sentence));

        // The same as the Break iterator into words
        // TODO: CHOOSE ONE SENTENCE BREAKER AND USE IN ALL PLACES
        List<CoreLabel> rawWords = tok.tokenize();

        try
        {
            Tree parse = lp.apply(rawWords);

            return parse.taggedYield();
        }catch (OutOfMemoryError x){
            System.err.println("ERROR: Sentence was probably too long:\n" + sentence);
            return null;
        }

        // TODO: POSSIBLY THIS CAN ALSO BE USED TO GET THE WORDNET RESULTS
        /*For instance, "penn" or "words,typedDependencies".
   *                Known formats are: oneline, penn, latexTree, xmlTree, words,
   *                wordsAndTags, rootSymbolOnly, dependencies,
   *                typedDependencies, typedDependenciesCollapsed,
   *                collocations, semanticGraph, conllStyleDependencies,
   *                conll2007.*/
        
        /*TreePrint tp = new TreePrint("collocations");
        System.out.println("tp.printTree(parse):");
        tp.printTree(parse);*/
    }

    /**
     * Returns the POS tag to use in the wordnet database,
     * according to the tag obtained from the Stanford Parser.
     *
     * @param tag obtained from the Stanford Parser
     *
     * @return the POS tag to use in the wordnet database
     */
    public POS getPOSWordnet(String tag)
    {
        char firstCharTag = tag.charAt(0);

        if (tag.length() < 2)
            return null;

        char secondCharTag = tag.charAt(1);

        // If the tag's first word is a J then the word is an adjective
        if (firstCharTag == 'J')
            return POS.ADJECTIVE;

            // If the tag's first word is a N then the word is a noun
        else if (firstCharTag == 'N')
            return POS.NOUN;

            // If the tag's first word is a V then the word is a verb
        else if (firstCharTag == 'V')
            return POS.VERB;

            // If the tag starts with "RB" then the word is an adverb
        else if (firstCharTag == 'R' && secondCharTag == 'B')
            return POS.ADVERB;
        else
            return null;
    }
}