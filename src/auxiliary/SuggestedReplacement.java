package auxiliary;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acácio Correia on 29/07/2016.
 */
public class SuggestedReplacement
{
    public static final int NUMBER_TYPES = 6;

    public static final int NONE = -1;
    public static final int LANGUAGE_TOOL = 0;
    public static final int SYNONYMS = 1;
    public static final int AFTER = 2;
    public static final int BETWEEN = 3;
    public static final int BEFORE = 4;
    public static final int PREPOSITIONS = 5;

    private List<Integer> types;
    private String replacement;

    public SuggestedReplacement(int type, String replacement)
    {
        this.types = new ArrayList<>();
        this.types.add(type);

        this.replacement = replacement;
    }

    public SuggestedReplacement(String replacement)
    {
        types = new ArrayList<>();

        this.replacement = replacement;
    }

    public List<Integer> getTypes()
    {
        return types;
    }

    public String getReplacement()
    {
        return replacement;
    }

    public void setTypes(List<Integer> types)
    {
        this.types = types;
    }

    public void addType(int type)
    {
        types.add(type);
    }

    public void setReplacement(String replacement)
    {
        this.replacement = replacement;
    }

    // This does not include the type because I'll basically just compare strings
    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SuggestedReplacement that = (SuggestedReplacement) o;

        return replacement != null ? replacement.equals(that.replacement) : that.replacement == null;

    }

    @Override
    public String toString()
    {
        return "SuggestedReplacement{" +
                "types=" + types +
                ", replacement='" + replacement + '\'' +
                '}';
    }
}