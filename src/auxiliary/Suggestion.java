package auxiliary;

import java.util.ArrayList;
import java.util.List;

/**
 * This was based on the RuleMatch class from the LanguageTool.
 * It represents a set of suggestions of replacements to a single word.
 *
 * @author Acácio Correia
 */
public class Suggestion
{
    private int fromPos;
    private int toPos;
    private String message;
    private List<SuggestedReplacement> replacements;

    private boolean hidden;

    public Suggestion(int fromPos, int toPos)
    {
        this(fromPos, toPos, new ArrayList<>());

        replacements = new ArrayList<>();
    }

    public Suggestion(int fromPos, int toPos, List<SuggestedReplacement> replacements)
    {
        this(fromPos, toPos, replacements, null);
    }

    public Suggestion(int fromPos, int toPos, List<SuggestedReplacement> replacements, String message)
    {
        this.fromPos = fromPos;
        this.toPos = toPos;
        this.replacements = replacements;
        this.message = message;

        hidden = true;
    }

    public int getFromPos()
    {
        return fromPos;
    }

    public int getToPos()
    {
        return toPos;
    }

    public String getMessage() { return message; }

    public List<SuggestedReplacement> getReplacements() { return replacements; }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setFromPos(int fromPos) { this.fromPos = fromPos; }

    public void setToPos(int toPos) { this.toPos = toPos; }

    public void setReplacements(List<SuggestedReplacement> replacements) { this.replacements = replacements; }

    public void setHidden(boolean hidden)
    {
        this.hidden = hidden;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Suggestion that = (Suggestion) o;

        if (fromPos != that.fromPos) return false;
        if (toPos != that.toPos) return false;
        if (!message.equals(that.message)) return false;
        return replacements.equals(that.replacements);

    }

    @Override
    public String toString()
    {
        return "Suggestion{" +
                "fromPos=" + fromPos +
                ", toPos=" + toPos +
                ", message='" + message + '\'' +
                ", replacements=" + replacements +
                '}';
    }

    /**
     * Get the position of the intended replacement in the list of suggested replacements or -1 if it doesn't exist.
     *
     * @param replacement the string you're looking for.
     * @param type        the type we are looking for.
     *
     * @return the position of the intended replacement in the list of suggested replacements or -1 if it doesn't exist.
     */
    public int getReplacementPositionInList(String replacement, int type)
    {
        SuggestedReplacement sr = new SuggestedReplacement(type, replacement);

        for(int i = 0; i < replacements.size(); i++)
            if(sr.equals(replacements.get(i)))
                return i;

        return -1;
    }

    public void addReplacementsFromType(List<String> suggestedReplacements, int type)
    {
        if(suggestedReplacements == null)
            return;

        int pos;

        for (String s : suggestedReplacements)
        {
            pos = replacements.indexOf(new SuggestedReplacement(SuggestedReplacement.NONE, s));

            // If this suggestions already exists then add the type
            if(pos != -1)
                replacements.get(pos).addType(type);
            // Otherwise add the suggestion
            else
                replacements.add(new SuggestedReplacement(type, s));
        }
    }
}