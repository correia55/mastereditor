package auxiliary;

import javafx.scene.control.TextArea;

import java.util.Objects;

/**
 * Used by the undo and redo systems to mark text changes.
 *
 * @author Acácio Correia
 */
public class TextChange
{
    private String oldValue;
    private String newValue;
    private TextArea t;

    private int startPosition;

    public TextChange(String oldValue, String newValue, TextArea t)
    {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.t = t;
    }

    public TextChange(String oldValue, String newValue, int startPosition)
    {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.startPosition = startPosition;
    }

    public void undo()
    {
        t.setText(oldValue);
    }

    public void redo()
    {
        t.setText(newValue);
    }

    public TextChange mergeWith(TextChange c)
    {
        newValue = c.newValue;
        return this;
    }

    public void setNewValue(String newValue)
    {
        this.newValue = newValue;
    }

    public String getOldValue()
    {
        return oldValue;
    }

    public String getNewValue()
    {
        return newValue;
    }

    public int getStartPosition()
    {
        return startPosition;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final TextChange other = (TextChange) obj;

        return Objects.equals(this.oldValue, other.oldValue);
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.oldValue);
        hash = 47 * hash + Objects.hashCode(this.newValue);
        return hash;
    }

    @Override
    public String toString()
    {
        return "TextChange{" + "oldValue=" + oldValue + ", newValue=" + newValue + ", t=" + t + ", startPosition=" + startPosition + '}';
    }
}