package mastereditor;

import auxiliary.*;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.SVGPath;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.io.*;
import java.net.URL;
import java.text.BreakIterator;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static javafx.application.Application.*;

/**
 * A text editor that helps users writing better scientific documents,
 * by comparing with other similar documents in the scientific community,
 * and providing suggestions accordingly.
 *
 * @author Acácio Correia
 */
public class FXMLDocumentController implements Initializable
{
    // FXML Variables
    // -------------------------------------------------------

    @FXML
    private CheckMenuItem cmiLineWrap;

    @FXML
    private MenuItem miFileNew;
    @FXML
    private MenuItem miFileOpen;
    @FXML
    private MenuItem miFileSave;
    @FXML
    private MenuItem miFileSaveAs;
    @FXML
    private MenuItem miFileClose;
    @FXML
    private MenuItem miFileExit;

    @FXML
    private MenuItem miEditUndo;
    @FXML
    private MenuItem miEditRedo;
    @FXML
    private MenuItem miEditCopy;
    @FXML
    private MenuItem miEditCut;
    @FXML
    private MenuItem miEditPaste;
    @FXML
    private MenuItem miEditFind;
    @FXML
    private MenuItem miEditReplace;
    @FXML
    private MenuItem miEditSelect;

    @FXML
    private MenuItem miSuggestionsCalculate;

    @FXML
    private MenuItem miSuggestionsConfiguration;


    @FXML
    private MenuItem miAbout;

    @FXML
    private CheckMenuItem cmiPrediction;

    @FXML
    private RadioMenuItem rmiPrefStyleCaspian;
    @FXML
    private RadioMenuItem rmiPrefStyleModena;

    @FXML
    private Button bPlus;
    @FXML
    private Button bFind;

    @FXML
    private Label lColumn;

    @FXML
    private ToggleButton tbCaseSensitive;
    @FXML
    private ToggleButton tbWholeWord;

    @FXML
    private TabPane tpText;

    @FXML
    private TextField tfFind;

    @FXML
    private ListView lvSuggestions;
    @FXML
    private ListView lvPredictions;

    // Settings
    // -------------------------------------------------------

    // The time to update the most relevant editorDocuments, in milliseconds
    // 60000 = 1 minute
    private final int UPDATE_TIME_TOPIC_DOCUMENTS = 300000;

    private final String predictionTableName = "statCut";
    private final boolean predictionFileSeparated = false;

    private final String suggestionTableName = "csTotal";
    private final boolean suggestionFileSeparated = false;

    private final int ngramSize = 3;
    private float fileThreshold = 3.0f;

    private boolean wordPrediction = true;

    private boolean verbose = true;

    // Topic Thread
    // -------------------------------------------------------

    // The thread responsible for the suggestions calculation
    private Thread topicThread;

    // The lock of the thread
    private static final class Lock {}

    private final Object lock = new Lock();

    // Other Variables
    // -------------------------------------------------------

    // The stage for this application
    private Stage stage;

    // The documents currently being edited
    private List<EditorDocument> editorDocuments;

    // The currently selected index
    private int selectedIndex;

    // Variables used in prediction
    private List<String> lastWords;
    private BreakIterator wordIterator;

    // Sentences
    private List<Sentence> sentences;
    private int currentSentence;
    private IndexRange selection;

    // Lists of items for the listviews
    private ObservableList<String> lvItems;
    private ObservableList<Sentence> sentenceItemList;

    // The class that treats the database requests
    // There is a need for two different database handlers because sometimes two requests were made simultaneously
    private NGramDB dbPredictions;
    private NGramDB dbSuggestions;

    private CalculateSuggestions cs;

    // The variable with the model of topics from the corpus
    private LDA topicsModel;

    private boolean unredoDone;
    private boolean textChanged;

    // The dialog for the suggestions configuration
    private Dialog suggestionsDialog;

    // The dialog for the about
    private Dialog aboutDialog;

    @Override
    public void initialize(URL url, ResourceBundle rb)
    {
        // Initialize the lists
        editorDocuments = new ArrayList<>();

        dbPredictions = new NGramDB(verbose);
        dbSuggestions = new NGramDB(verbose);

        wordIterator = BreakIterator.getWordInstance(new Locale("en", "GB"));

        cs = new CalculateSuggestions(suggestionTableName, suggestionFileSeparated, verbose, dbSuggestions);

        // Get the lists of items from the listviews
        lvItems = lvPredictions.getItems();

        sentenceItemList = lvSuggestions.getItems();

        // Treat mouse click on an item inside the suggestions listview
        lvSuggestions.setCellFactory(new Callback<ListView<ListView>, ListCell<String>>()
        {
            @Override
            public ListCell<String> call(ListView<ListView> list)
            {
                return new MouseClickListCell();
            }
        });

        unredoDone = false;
        textChanged = false;

        setDefaulPreferences();

        // When the tab index changes
        tpText.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) ->
        {
            selectedIndex = newValue.intValue();

            // The selected tab is the plus one so do nothing
            if (newValue.intValue() == tpText.getTabs().size() - 1)
                return;

            if (editorDocuments.size() > selectedIndex)
                updateSuggestionsListView();

            // Update the pointer to sentences when the document is not a new one
            if(selectedIndex < editorDocuments.size())
            {
                EditorDocument e = editorDocuments.get(selectedIndex);
                sentences = e.getSentences();
            }
        });

        // Activate or deactivate linewrap according to the value of CMILineWrap's property
        cmiLineWrap.selectedProperty().addListener((observable, oldValue, newValue) ->
        {
            for (EditorDocument d : editorDocuments)
            {
                d.getTextA().setWrapText(newValue);
            }
        });

        // Activate or deactivate word prediction
        cmiPrediction.selectedProperty().addListener((observable, oldValue, newValue) ->
                wordPrediction = cmiPrediction.isSelected());

        // When you click the new item on the file menu
        // create a new tab
        miFileNew.setOnAction(e -> bPlus.fire());

        // When you click the open item on the file menu
        // open a dialog for selecting the text file
        miFileOpen.setOnAction(e -> openTextFile());

        // When you click the save item on the file menu
        miFileSave.setOnAction(e -> saveTextFile());

        // When you click the save as item on the file menu
        miFileSaveAs.setOnAction(e ->
        {
            // The selected tab is the plus one so do nothing
            if (selectedIndex == tpText.getTabs().size() - 1)
                return;

            saveTextFileAs(selectedIndex);
        });

        // When you click the open item on the file menu
        // open a dialog for selecting the text file
        miFileClose.setOnAction(e -> closeTextFile());

        // When you click the exit as item on the file menu
        miFileExit.setOnAction(e ->
        {
            beforeClosing();
            Platform.exit();
        });

        // When you click the undo item on the edit menu
        miEditUndo.setOnAction(e -> undo());

        // When you click the redo item on the edit menu
        miEditRedo.setOnAction(e -> redo());

        // When you click the copy item on the edit menu
        miEditCopy.setOnAction(e -> editorDocuments.get(selectedIndex).getTextA().copy());

        // When you click the select all item on the edit menu
        miEditSelect.setOnAction(e -> editorDocuments.get(selectedIndex).getTextA().selectAll());

        // When you click the cut item on the edit menu
        miEditCut.setOnAction(event ->
        {
            selection = editorDocuments.get(selectedIndex).getTextA().getSelection();

            treatSelection();

            printAllSentences();

            editorDocuments.get(selectedIndex).getTextA().cut();
        });

        miEditPaste.setOnAction(event ->
        {
            currentSentence = getCurrentSentence(selection.getStart());

            selection = editorDocuments.get(selectedIndex).getTextA().getSelection();

            treatPaste();

            editorDocuments.get(selectedIndex).getTextA().paste();
        });

        // When you click the find item on the edit menu
        miEditFind.setOnAction(e -> tfFind.requestFocus());

        // When you click the calculate suggestions button on the suggestions menu
        miSuggestionsCalculate.setOnAction(e ->
        {
            synchronized (lock)
            {
                lock.notify();
            }
        });

        // When you click the configure suggestions button show a dialog
        miSuggestionsConfiguration.setOnAction(e ->
        {
            synchronized (lock)
            {
                Optional<Float> proximity = suggestionsDialog.showAndWait();

                proximity.ifPresent(x -> fileThreshold = x);
            }
        });

        // When you click the about button
        miAbout.setOnAction(e ->
                aboutDialog.showAndWait());

        // When you click the style caspian item on the preferences menu
        rmiPrefStyleCaspian.setOnAction(e ->
        {
            setUserAgentStylesheet(STYLESHEET_CASPIAN);

            editorDocuments.forEach((d) ->
                    d.getTextA().setStyle("-fx-background-color: black; -fx-text-fill: white;"));
        });

        // When you click the style modena item on the preferences menu
        rmiPrefStyleModena.setOnAction(e ->
        {
            setUserAgentStylesheet(STYLESHEET_MODENA);

            editorDocuments.forEach((d) ->
                    d.getTextA().setStyle("-fx-text-fill: black;"));
        });

        // Make the plus button transparent
        bPlus.setStyle("-fx-background-color: transparent;");

        // When you click the plus button
        bPlus.setOnAction(e -> openUntitledTab());

        // When you click the find button
        bFind.setOnAction(e -> textFind(tfFind.getText()));

        // Activate the find button on Enter
        tfFind.setOnAction(e -> bFind.fire());

        // Remove the default context menu
        tfFind.setContextMenu(new ContextMenu());

        // Create a tab if no other is opened
        bPlus.fire();

        createConfigurationDialog();
        createAboutDialog();

        // Triggers an update to the most similar editorDocuments every 'UPDATE_TIME_TOPIC_DOCUMENTS' milliseconds
        updateTopicDocuments();
    }

    /**
     * Creates and focus on a new tab.
     *
     * @param tabName       the new tab name.
     * @param indexPosition the position where tab is placed.
     */
    private void createNewTab(String tabName, int indexPosition)
    {
        TextArea textArea = createTextArea();

        // Remove the default context menu
        textArea.setContextMenu(new ContextMenu());

        // Forces the text area to fit the whole tab size
        AnchorPane.setTopAnchor(tpText, 0.0);
        AnchorPane.setBottomAnchor(tpText, 0.0);
        AnchorPane.setLeftAnchor(tpText, 0.0);
        AnchorPane.setRightAnchor(tpText, 0.0);

        BorderPane bp = new BorderPane();
        bp.setCenter(textArea);

        AnchorPane anchor = new AnchorPane(bp);

        // Forces the text area to fit the whole tab size
        AnchorPane.setTopAnchor(bp, 0.0);
        AnchorPane.setBottomAnchor(bp, 0.0);
        AnchorPane.setLeftAnchor(bp, 0.0);
        AnchorPane.setRightAnchor(bp, 0.0);

        Tab newTab = new Tab(tabName, anchor);

        // Add the new tab
        tpText.getTabs().add(indexPosition, newTab);

        // Select the newly created tab
        SingleSelectionModel<Tab> selectionModel = tpText.getSelectionModel();
        selectionModel.select(newTab);

        textArea.requestFocus();

        EditorDocument e = new EditorDocument(textArea);

        // Create a new document with the associated textArea
        editorDocuments.add(e);

        sentences = e.getSentences();
        sentences.add(new Sentence("", 0, 0));

        currentSentence = sentences.size() - 1;
    }

    /**
     * Create a text area.
     *
     * @return the text area.
     */
    private TextArea createTextArea()
    {
        TextArea textA = new TextArea();

        lastWords = new ArrayList<>();

        EventHandler keyPressedFilter = createKeyPressedFilter();

        textA.addEventFilter(KeyEvent.KEY_PRESSED, keyPressedFilter);

        EventHandler keyTypedFilter = createKeyTypedFilter();

        textA.addEventFilter(KeyEvent.KEY_TYPED, keyTypedFilter);

        // When the text in the textarea changes
        textA.textProperty().addListener((observable, oldValue, newValue) ->
        {
            // Prevents saving the changes when the difference in the size of the strings is bigger than 1 character
            // which is supposed to prevent changes after undoing or redoing
            if (Math.abs(oldValue.length() - newValue.length()) > 1)
                return;

            if (unredoDone)
                unredoDone = false;
            else
            {
                TextChange newChange = new TextChange(oldValue, newValue, editorDocuments.get(selectedIndex).getTextA());

                Stack<TextChange> undoStack = editorDocuments.get(selectedIndex).getUndoStack();

                if (textChanged && !undoStack.isEmpty())
                {
                    undoStack.get(undoStack.size() - 1).mergeWith(newChange);
                    textChanged = false;
                }
                else
                    undoStack.add(newChange);
            }
        });

        // When the caret position changes
        textA.caretPositionProperty().addListener((observable, oldValue, newValue) ->
        {
            // Write the current column
            lColumn.setText("Column " + newValue);
        });

        // Make it line wrap accordingly to cmiLineWrap
        textA.setWrapText(cmiLineWrap.isSelected());

        if (getUserAgentStylesheet().equals(STYLESHEET_CASPIAN))
        {
            textA.setStyle("-fx-background-color: black; -fx-text-fill: white;");
        }
        else
        {
            textA.setStyle("-fx-text-fill: black;");
        }

        return textA;
    }

    /**
     * The event filter responsible for treating the KeyEvent.KEY_PRESSED.
     * This includes:
     * - interacting with the predictions list view when it is visible;
     * - undo and redo actions.
     *
     * @return the filter created.
     */
    private EventHandler createKeyPressedFilter()
    {
        return (EventHandler<KeyEvent>) event ->
        {
            // Get the current selection
            selection = editorDocuments.get(selectedIndex).getTextA().getSelection();

            KeyCode key = event.getCode();

            // If the list is visible
            if (lvPredictions.isVisible())
            {
                // Enter is to apply the prediction
                if (key.equals(KeyCode.ENTER))
                {
                    applyPrediction((TextArea) event.getTarget());
                    event.consume();
                }

                // Up changes the selected item up
                else if (key.equals(KeyCode.UP))
                {
                    MultipleSelectionModel selectionModel = lvPredictions.getSelectionModel();

                    selectionModel.selectPrevious();
                }

                // Down changes the selected item down
                else if (key.equals(KeyCode.DOWN))
                {
                    MultipleSelectionModel selectionModel = lvPredictions.getSelectionModel();

                    selectionModel.selectNext();
                }
            }

            if (event.isControlDown())
            {
                // Ctrl+z -> UNDO
                if (event.getCode() == KeyCode.Z)
                {
                    undo();
                }

                // Ctrl+y -> REDO
                else if (event.getCode() == KeyCode.Y)
                {
                    redo();
                }
            }
        };
    }

    /**
     * The event filter responsible for treating the KeyEvent.KEY_TYPED.
     * This includes:
     * - saving the letters to a string used in word prediction;
     * - updating the predictions list when a character is introduced.
     *
     * @return the filter created.
     */
    private EventHandler createKeyTypedFilter()
    {
        return (EventHandler<KeyEvent>) event ->
        {
            treatSentences(event);

            predictWords();
        };
    }

    private void printAllSentences()
    {
        System.out.println("------------------");

        sentences.forEach(System.out::println);
    }

    private void treatPaste()
    {
        treatSelection();

        int start = sentences.get(currentSentence).getStartPos();

        String text = sentences.get(currentSentence).getSentence();
        int sentColumn = selection.getStart() - start;

        String pasteText = Clipboard.getSystemClipboard().getString().replaceAll("\r", "");

        // The combination of the current sentence and the clipboard text
        // The \r need to be replaced with nothing, otherwise delete will delete them first
        text = text.substring(0, sentColumn) + pasteText + text.substring(sentColumn, text.length());

        sentences.remove(currentSentence);
        createSentences(text, start);

        updateAllSentences();

        printAllSentences();

        // Update the current selection
        selection = new IndexRange(selection.getStart() + pasteText.length(), selection.getStart() + pasteText.length());
    }

    private void createSentences(String text, int startPos)
    {
        // Needed in order to force the creation of a new sentence when needed
        if (currentSentence == sentences.size())
            text += " ";

        String[] texts = text.split("(?<=[.!?])");

        // Remove the last character of the last sentence
        if (currentSentence == sentences.size())
            texts[texts.length - 1] = texts[texts.length - 1].substring(0, texts[texts.length - 1].length() - 1);

        Sentence s;

        for (int i = 0; i < texts.length; i++)
        {
            s = new Sentence(texts[i], startPos, startPos + texts[i].length());
            sentences.add(currentSentence + i, s);

            startPos += texts[i].length();
        }

        currentSentence += texts.length - 1;
    }

    /**
     * Get the first and last sentences that are involved by the current selection.
     *
     * @param selection the text selection.
     *
     * @return the list with the first and last that are involved by the current selection.
     */
    private List<Integer> getAllInvolvedSentences(IndexRange selection)
    {
        List<Integer> involvedSentences = new ArrayList<>();

        Sentence s;

        boolean conflict;

        for (int i = 0; i < sentences.size(); i++)
        {
            s = sentences.get(i);

            conflict = false;

            // Not yet involved
            if (s.getEndPos() < selection.getStart())
                continue;

            // No longer involved
            if (s.getStartPos() > selection.getEnd())
                break;

            // c1 selectionStart
            if (selection.getStart() >= s.getStartPos())
            {
                // c1 selectionStart c2 -> Conflict
                if (selection.getStart() < s.getEndPos())
                {
                    conflict = true;
                }
                // else c1 c2 selectionStart selectionEnd -> No effect
                // s1 c1
            }
            else
            {
                // s1 c1 s2 -> Conflict
                if (selection.getEnd() >= s.getStartPos())
                {
                    conflict = true;
                }
                // else s1 s2 c1 c2 -> No effect
            }

            if (conflict)
            {
                if (involvedSentences.size() == 2)
                    involvedSentences.remove(1);

                involvedSentences.add(i);
            }
        }

        // There can only be one sentence when there's no selection involved
        if (selection.getStart() == selection.getEnd() && involvedSentences.size() == 2)
            involvedSentences.remove(0);

        return involvedSentences;
    }

    private int getCurrentSentence(int column)
    {
        boolean foundCandidate = false;

        if (column == 0)
            return 0;

        for (int i = 0; i < sentences.size(); i++)
        {
            // If this sentence is a candidate
            if (sentences.get(i).getStartPos() <= column && sentences.get(i).getEndPos() >= column)
            {
                // There are two candidates so its the second one
                if (foundCandidate)
                {
                    return i;
                }
                else
                    foundCandidate = true;
            }
            // The current isn't a candidate but there was one before which is the correct one
            else if (foundCandidate)
                return (i - 1);
        }

        if (foundCandidate)
            return (sentences.size() - 1);

        System.err.println("ERROR in getCurrentSentence: There is no current sentence for column " + column + ".");

        return -1;
    }

    /**
     * Update the positions of the next sentences.
     */
    private void updateAllSentences()
    {
        if (sentences.isEmpty())
            return;

        sentences.get(0).setEndPos(sentences.get(0).getSentence().length());

        // Update the positions of the sentences that come after
        for (int i = 1; i < sentences.size(); i++)
        {
            sentences.get(i).setStartPos(sentences.get(i - 1).getEndPos());
            sentences.get(i).setEndPos(sentences.get(i - 1).getEndPos() + sentences.get(i).getSentence().length());
        }
    }

    /**
     * Erases the selection and unites the sentences involved by the selection.
     *
     * @return true if there was a selection and it was erased.
     */
    private boolean treatSelection()
    {
        String sent;

        // There's no selection
        if (selection.getStart() == selection.getEnd())
            return false;

        List<Integer> involvedSentences = getAllInvolvedSentences(selection);

        // Probably something wrong
        if (involvedSentences.isEmpty())
        {
            System.err.println("WARNING in treatSentences: the selection must involve at least one sentence.");
        }
        // Involves only one sentence
        else if (involvedSentences.size() == 1)
        {
            currentSentence = involvedSentences.get(0);

            sent = sentences.get(currentSentence).getSentence();

            sent = sent.substring(0, selection.getStart() - sentences.get(currentSentence).getStartPos()) + sent.substring(selection.getEnd() - sentences.get(currentSentence).getStartPos(), sent.length());

            sentences.get(currentSentence).setSentence(sent);
            sentences.get(currentSentence).setWasEdited(true);

            // Remove the suggestions
            sentences.get(currentSentence).getSuggestions().clear();
            updateSuggestionsListView();

            updateAllSentences();
        }
        // Involves more than one sentence
        else
        {
            int startS = involvedSentences.get(0);
            int endS = involvedSentences.get(1);

            currentSentence = startS;

            Sentence endSentence = sentences.get(endS);

            sent = sentences.get(currentSentence).getSentence();

            //sent = sent.substring(0, selection.getStart() - sentences.get(currentSentence).getStartPos()) + event.getCharacter();
            sent = sent.substring(0, selection.getStart() - sentences.get(currentSentence).getStartPos());
            sent += endSentence.getSentence().substring(selection.getEnd() - endSentence.getStartPos(), endSentence.getSentence().length());

            sentences.get(currentSentence).setSentence(sent);
            sentences.get(currentSentence).setWasEdited(true);

            // Remove the suggestions
            sentences.get(currentSentence).getSuggestions().clear();
            updateSuggestionsListView();

            for (int i = 0; i < endS - startS; i++)
                sentences.remove(startS + 1);

            updateAllSentences();
        }

        return true;
    }

    /**
     * Split a sentence into two, at the referred location.
     *
     * @param column the location of the split.
     */
    private void splitSentence(int column)
    {
        // The original sentence
        String originalSent = sentences.get(currentSentence).getSentence();

        // The new next sentence
        String sent = originalSent.substring(column + 1, originalSent.length());

        // The new previous sentence
        originalSent = originalSent.substring(0, column + 1);

        // Update the previous sentence and positions
        sentences.get(currentSentence).setSentence(originalSent);
        sentences.get(currentSentence).setEndPos(originalSent.length() + sentences.get(currentSentence).getStartPos());

        Sentence newS = new Sentence(sent, sentences.get(currentSentence).getEndPos(), sentences.get(currentSentence).getEndPos() + sent.length());

        sentences.add(currentSentence + 1, newS);

        currentSentence++;
    }

    private void treatSentences(KeyEvent event)
    {
        currentSentence = getCurrentSentence(selection.getStart());

        String sent;

        int sentColumn;

        String c = event.getCharacter();

        // A word character
        if (c.matches("[0-9A-Za-z_.,!'-?#<> \r\n\t]"))
        {
            if(event.isControlDown())
                return;

            if (!treatSelection())
                currentSentence = getCurrentSentence(selection.getStart());

            System.out.println(c.charAt(0) + " -> " + c);

            // Makes sure the correct line breaker is used
            if (c.matches("[\r\n]"))
                c = "\n";

            sent = sentences.get(currentSentence).getSentence();
            sentColumn = selection.getStart() - sentences.get(currentSentence).getStartPos();

            // Adds the new character that was introduced to the correct sentence location
            if (sent.length() > 0)
                sent = sent.substring(0, sentColumn) + c + sent.substring(sentColumn, sent.length());
            else
                sent = event.getCharacter();

            sentences.get(currentSentence).setSentence(sent);
            sentences.get(currentSentence).setEndPos(sent.length() + sentences.get(currentSentence).getStartPos());
            sentences.get(currentSentence).setWasEdited(true);

            // Remove the suggestions
            sentences.get(currentSentence).getSuggestions().clear();
            updateSuggestionsListView();

            if (event.getCharacter().matches("[.!?]"))
            {
                if (currentSentence == sentences.size() - 1)
                {
                    // If this is the last character then create a new sentence after this one
                    if (sentColumn + 1 == sentences.get(currentSentence).getSentence().length())
                    {
                        sentences.add(new Sentence("", sentences.get(currentSentence).getEndPos(), sentences.get(currentSentence).getEndPos()));
                        currentSentence++;
                    }
                    else
                        splitSentence(sentColumn);
                }
                else
                    splitSentence(sentColumn);
            }

            selection = new IndexRange(selection.getStart() + 1, selection.getStart() + 1);

            updateAllSentences();

            printAllSentences();
        }
        else
        {
            int start = selection.getStart();

            switch ((int) event.getCharacter().charAt(0))
            {
                case 127:   // DELETE
                    if (start + 1 > sentences.get(sentences.size() - 1).getEndPos())
                        return;

                    start++;
                case 8:     // BACK_SPACE
                    // If there is no selection we need to erase the previous character
                    if (!treatSelection())
                    {
                        currentSentence = getCurrentSentence(start);

                        sent = sentences.get(currentSentence).getSentence();
                        sentColumn = start - sentences.get(currentSentence).getStartPos();

                        // Then it belongs to the previous sentence
                        if (sentColumn - 1 == -1)
                        {
                            currentSentence--;

                            // When its the first sentence, there's no previous sentence
                            if (currentSentence == -1)
                                break;

                            sentences.get(currentSentence).setWasEdited(true);

                            // Remove the suggestions
                            sentences.get(currentSentence).getSuggestions().clear();
                            updateSuggestionsListView();

                            sent = sentences.get(currentSentence).getSentence();
                            sentColumn = selection.getStart() - sentences.get(currentSentence).getStartPos();

                            String aux = sent.substring(0, sentColumn - 1);

                            if (sentColumn + 1 < sent.length())
                                aux += sent.substring(sentColumn + 1, sent.length());

                            sent = aux;

                            sent += sentences.get(currentSentence + 1).getSentence();

                            sentences.remove(currentSentence + 1);
                        }
                        else
                        {
                            sentences.get(currentSentence).setWasEdited(true);

                            // Remove the suggestions
                            sentences.get(currentSentence).getSuggestions().clear();
                            updateSuggestionsListView();

                            String aux = sent.substring(0, sentColumn - 1);

                            aux += sent.substring(sentColumn, sent.length());

                            sent = aux;
                        }

                        sentences.get(currentSentence).setSentence(sent);

                        updateAllSentences();

                        selection = new IndexRange(selection.getStart() - 1, selection.getStart() - 1);
                    }

                    printAllSentences();

                    break;

                case 24:   // CTRL + x or CTRL + SHIFT + X
                    if (!event.isControlDown())
                        break;

                    if (event.isShiftDown())
                        break;

                    // Ctrl + x

                    treatSelection();

                    printAllSentences();

                    break;

                case 22:   // CTRL + v or CTRL + SHIFT + v
                    if (!event.isControlDown())
                        break;

                    if (event.isShiftDown())
                        break;

                    // Ctrl + v

                    treatPaste();

                    break;
            }
        }
    }

    /**
     * Applies the selected word from the prediction list.
     *
     * @param t the textArea in which to apply the change.
     */
    private void applyPrediction(TextArea t)
    {
        String text = t.getText();

        int carPosition = t.getCaretPosition();

        String newWord = lvItems.get(lvPredictions.getSelectionModel().getSelectedIndex());

        t.setText(text.substring(0, carPosition - lastWords.get(lastWords.size() - 1).length()) + newWord + " "
                + text.substring(carPosition, text.length()));

        t.positionCaret(carPosition + newWord.length() + 1);

        // Apply the prediction to the lastWords list
        lastWords.set(lastWords.size() - 1, newWord);

        lvPredictions.setVisible(false);
    }

    /**
     * Create a dialog box for the configuration of the suggestions.
     */
    private void createConfigurationDialog()
    {
        // Create the custom dialog.
        suggestionsDialog = new Dialog();
        suggestionsDialog.setTitle("Suggestions Configuration");

        // Set the button types.
        ButtonType dialogButtons = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        suggestionsDialog.getDialogPane().getButtonTypes().addAll(dialogButtons, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(30, 10, 10, 10));

        Label proximityValue = new Label("3,00");

        Slider proximitySlider = new Slider(0, 5, 3);

        proximitySlider.valueProperty().addListener((ov, old_val, new_val) ->
                proximityValue.setText(String.format("%.2f", new_val.floatValue())));

        grid.add(new Label("Proximity:"), 0, 0);
        grid.add(proximitySlider, 1, 0);

        grid.add(proximityValue, 2, 0);

        suggestionsDialog.getDialogPane().setContent(grid);

        // Convert the result to a username-password-pair when the login button is clicked.
        suggestionsDialog.setResultConverter(dialogButton ->
        {
            if (dialogButton == dialogButtons)
            {
                return (float) proximitySlider.getValue();
            }

            return null;
        });

        // Request focus on the username field by default.
        Platform.runLater(proximitySlider::requestFocus);
    }

    /**
     * Create a dialog box for the about.
     */
    private void createAboutDialog()
    {
        // Create the custom dialog.
        aboutDialog = new Dialog();
        aboutDialog.setTitle("About");

        // Set the button types.
        ButtonType dialogButtons = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
        aboutDialog.getDialogPane().getButtonTypes().add(dialogButtons);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(30, 10, 10, 10));

        Label text = new Label("Developed by Acácio Correia as the dissertation for a Master's Degree in Computer Science and Engineering at the University of Beira Interior. " +
                "This work was done under the supervision and advisement of João Paulo Cordeiro (Ph.D.) and Pedro R. M. Inácio (Ph.D.).");

        text.setPrefWidth(300);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setWrapText(true);

        grid.add(text, 0, 0);

        aboutDialog.getDialogPane().setContent(grid);
    }

    /**
     * Update the suggestions displayed at the list view.
     */
    private void updateSuggestionsListView()
    {
        Platform.runLater(() ->
        {
            sentenceItemList.clear();

            // The text in that TextArea
            final String text = getTextSelectedTab();

            if (text == null)
                return;

            int i;

            // Iterate over all the existing suggestions
            for (Sentence sentence : sentences)
                for (i = 0; i < sentence.getSuggestions().size(); i++)
                    sentenceItemList.add(sentence);
        });
    }

    /**
     * Opens a text file in a new tab.
     */
    private void openTextFile()
    {
        FileChooser f = new FileChooser();
        f.setTitle("Open Text File");

        // Add a .txt extension to the file chooser
        f.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text files (.txt)", "*.txt"));

        File chosenFile = f.showOpenDialog(stage);

        // If a file was chosen
        if (chosenFile == null)
            return;

        createNewTab(chosenFile.getPath(), tpText.getTabs().size() - 1);

        selection = editorDocuments.get(selectedIndex).getTextA().getSelection();

        // http://stackoverflow.com/questions/4716503/best-way-to-read-a-text-file-in-java
        try (BufferedReader br = new BufferedReader(new FileReader(chosenFile)))
        {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null)
            {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }

            br.close();

            String fileText = sb.toString();

            ClipboardContent cc = new ClipboardContent();
            cc.putString(fileText);

            Clipboard.getSystemClipboard().setContent(cc);

            treatPaste();

            editorDocuments.get(selectedIndex).getTextA().paste();

            //editorDocuments.get(tpText.getTabs().size() - 2).setText(fileText);
        }
        catch (IOException e)
        {
            System.err.println("ERROR: Reading file - " + e);
        }
    }

    /**
     * Opens a new untitled tab for quickly writing ideas.
     */
    private void openUntitledTab()
    {
        int tabNumber = 1;
        createNewTab("Untitled Tab " + tabNumber, tpText.getTabs().size() - 1);
    }

    /**
     * Save the selected tab to a text file.
     */
    private void saveTextFile()
    {
        // The selected tab is the plus one so do nothing
        if (selectedIndex == tpText.getTabs().size() - 1)
            return;

        String tabName = (tpText.getSelectionModel().getSelectedItem()).getText();

        if (tabName.charAt(0) == 'U' && tabName.contains("Untitled Tab "))
            saveTextFileAs(selectedIndex);
        else
        {
            File chosenFile = new File(tabName);

            saveTextFileInPath(selectedIndex, chosenFile);
        }
    }

    /**
     * Open a file chooser and save a file at the chosen location.
     *
     * @param selectedIndex the index of the selected tab.
     */
    private void saveTextFileAs(int selectedIndex)
    {
        FileChooser f = new FileChooser();
        f.setTitle("Save Text File");

        // Add a .txt extension to the file chooser
        f.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text files (.txt)", "*.txt"));

        File chosenFile = f.showSaveDialog(stage);

        // If the path to save was chosen
        if (chosenFile == null)
            return;

        File finalFile;

        // Ensures that the extension is .txt
        // If it does not contain .txt then add it
        if (!chosenFile.getAbsolutePath().contains(".txt"))
            finalFile = new File(chosenFile.getAbsolutePath() + ".txt");
        else
            finalFile = chosenFile;

        saveTextFileInPath(selectedIndex, finalFile);
    }

    /**
     * Save file in the desired path.
     *
     * @param selectedIndex  the index of the selected tab.
     * @param fileDescriptor the file descriptor of the file to save.
     */
    private void saveTextFileInPath(int selectedIndex, File fileDescriptor)
    {
        try (PrintWriter writer = new PrintWriter(fileDescriptor))
        {
            writer.write(editorDocuments.get(selectedIndex).getText());

            tpText.getTabs().get(selectedIndex).setText(fileDescriptor.getPath());
        }
        catch (FileNotFoundException e)
        {
            System.err.println("ERROR: Saving file - " + e);
        }
    }

    /**
     * Close the currently selected window.
     */
    private void closeTextFile()
    {
        ObservableList<Tab> tabs = tpText.getTabs();

        // If the selected tab is the plus one prevent closing it
        if (selectedIndex == tabs.size() - 1)
            return;

        // Add the new tab
        tabs.remove(selectedIndex);

        // Remove the associated document
        editorDocuments.remove(selectedIndex);

        if (!tabs.isEmpty())
        {
            // Select the tab before the one just closed
            SingleSelectionModel<Tab> selectionModel = tpText.getSelectionModel();
            selectionModel.select(tabs.get(Math.max(selectedIndex - 1, 0)));
        }
    }

    /**
     * The function responsible for taking care of the undo.
     */
    private void undo()
    {
        if (editorDocuments.get(selectedIndex).getUndoStack().isEmpty())
        {
            //System.out.println("Undo Stack is empty.");
            return;
        }

        unredoDone = true;

        TextChange t = editorDocuments.get(selectedIndex).getUndoStack().pop();
        t.undo();

        editorDocuments.get(selectedIndex).getTextA().positionCaret(t.getOldValue().length());

        editorDocuments.get(selectedIndex).getRedoStack().add(t);
    }

    /**
     * The function responsible for taking care of the redo.
     */
    private void redo()
    {
        Stack<TextChange> redoStack = editorDocuments.get(selectedIndex).getRedoStack();

        if (redoStack.isEmpty())
        {
            //System.out.println("Redo Stack is empty.");
            return;
        }

        unredoDone = true;

        TextChange t = redoStack.pop();
        t.redo();

        editorDocuments.get(selectedIndex).getTextA().positionCaret(t.getOldValue().length());

        editorDocuments.get(selectedIndex).getUndoStack().add(t);
    }

    /**
     * Find a certain substring in the document.
     *
     * @param substring we want to find.
     */
    private void textFind(String substring)
    {
        // The selected tab is the plus one so do nothing
        if (selectedIndex == tpText.getTabs().size() - 1)
            return;

        TextArea tArea = editorDocuments.get(selectedIndex).getTextA();

        String text = tArea.getText();

        boolean caseSensitive = tbCaseSensitive.isSelected();

        int currentPosition = tArea.getCaretPosition();

        // If case sensitive is false
        if (!caseSensitive)
        {
            text = text.toLowerCase();
            substring = substring.toLowerCase();
        }

        boolean substringComplete = tbWholeWord.isSelected();

        int pos = -1;

        // http://whyjava.wordpress.com/2010/05/04/finding-all-the-indexes-of-a-whole-word-in-a-given-string-using-java/
        // If it should only look for the complete substring 
        if (substringComplete)
        {
            Matcher matcher = Pattern.compile("\\b" + substring + "\\b").matcher(text);

            if (matcher.find(currentPosition))
                pos = matcher.start();
        }
        else
        {
            pos = text.indexOf(substring, currentPosition);
        }

        if (pos != -1)
        {
            tArea.positionCaret(pos);
            tArea.selectPositionCaret(pos + substring.length());
        }
    }

    /**
     * Responsible for updating the most relevant editorDocuments in terms of topics of the text being written.
     */
    private void updateTopicDocuments()
    {
        topicThread = new Thread()
        {
            public void run()
            {
                try
                {
                    topicsModel = new LDA(false);

                    topicsModel.importSavedState("D:\\Bibliotecas\\Documentos\\NetBeansProjects\\MasterEditor\\resources\\corpusState.txt");

                    long startTime;
                    String newText;

                    while (true)
                    {
                        synchronized (lock)
                        {
                            lock.wait(UPDATE_TIME_TOPIC_DOCUMENTS);
                        }

                        startTime = System.currentTimeMillis();

                        // The text in the selected text area
                        newText = getTextSelectedTab();

                        if (newText == null)
                            return;

                        // If the text is empty
                        if (newText.equals(""))
                            return;

                        topicModelDocuments(newText);

                        // Calculates the suggestions and shows them on the view
                        getTabTextSuggestions();
                        updateSuggestionsListView();

                        System.out.println("Suggestion Update took " + (System.currentTimeMillis() - startTime) + "ms");
                    }
                }
                catch (InterruptedException v)
                {
                    System.err.println("ERROR in updateTopicDocuments: " + v);
                }
            }
        };

        topicThread.start();
    }

    /**
     * Calculate the most relevant editorDocuments to a text.
     *
     * @param text being analyzed.
     */
    private void topicModelDocuments(String text)
    {
        // The topic distribution for the new document
        double[] topicDistribution = topicsModel.distributionNewDocument(text);

        // Add the closest documents to the current document
        editorDocuments.get(selectedIndex).setContext(new FileContext(topicsModel.getDocumentsUnderThreshold(topicDistribution, fileThreshold)));

        System.out.println("Closest documents obtained!");
    }

    /**
     * Get the text from the selected tab.
     *
     * @return the text from the selected tab.
     */
    private String getTextSelectedTab()
    {
        // The selected tab is the plus one so do nothing
        if (selectedIndex == tpText.getTabs().size() - 1)
            return null;

        return (editorDocuments.get(selectedIndex).getText());
    }

    /**
     * Get the suggestions for the text in the selected tab.
     * These are based on the synonyms of each word obtained from the wordnet and the n-grams.
     */
    private void getTabTextSuggestions()
    {
        long startTime = System.currentTimeMillis();

        String text = getTextSelectedTab();

        if (text == null)
            return;

        // If the text is empty
        if (text.equals(""))
            return;

        cs.setDocument(editorDocuments.get(selectedIndex));
        cs.ngramWordnetSuggestions(sentences, true, true);

        System.out.println("getTabTextSuggestions took " + (System.currentTimeMillis() - startTime) + "ms");

        printAllSentences();
    }

    private void predictWords()
    {
        if(!wordPrediction)
            return;

        getPreviousWords(selection.getStart());

        List<String> nextWords;

        if (lastWords.size() == ngramSize - 1)
            nextWords = dbPredictions.getMostLikelyWordsOfInterest(lastWords, null, null, (byte) 3, null, predictionTableName, predictionFileSeparated);
        else
            nextWords = dbPredictions.getMostLikelyWords(lastWords, (byte) 3, predictionTableName, predictionFileSeparated);

        if (nextWords != null)
        {
            // Remove tags
            for (int i = 1; i < ngramSize; i++)
                nextWords.remove("<string" + i + ">");

            nextWords.remove("<stringn>");

            for (int i = ngramSize - 2; i > 0; i--)
                nextWords.remove("<stringn-" + i + ">");

            if(!nextWords.isEmpty())
            {
                // Remove all strings from the word prediction
                lvItems.clear();

                lvItems.addAll(nextWords);
                lvPredictions.setVisible(true);

                // This creates a new thread to prevent the lagging in the writing
                Platform.runLater(() -> lvPredictions.getSelectionModel().select(0));
            }
            else
                lvPredictions.setVisible(false);
        }
        else
            lvPredictions.setVisible(false);
    }

    private void getPreviousWords(int c)
    {
        lastWords.clear();

        // Adds strings that represent the beginning of the sentence
        for (int i = 1; i < ngramSize; i++)
            lastWords.add("<string" + i + ">");

        int cs = getCurrentSentence(c);

        // Probably something wrong
        if (cs == -1)
        {
            System.err.println("WARNING in getPreviousWords: the selection must involve at least one sentence.");
        }
        // Involves at least one sentence, which is the one I care about
        else
        {
            Sentence sent = sentences.get(cs);

            int column = c - sent.getStartPos();

            String text = sent.getSentence();

            wordIterator.setText(text);
            int start = wordIterator.first();

            // Iterate over the words of the text until we get to the column
            for (int end = wordIterator.next();
                 end != BreakIterator.DONE;
                 start = end, end = wordIterator.next())
            {
                // It is a new word
                if(start > column)
                    break;

                if(text.substring(start, end).split(" ").length == 0)
                    continue;

                lastWords.add(text.substring(start, end));

                if(lastWords.size() > ngramSize)
                    lastWords.remove(0);

                if(end >= column)
                    return;
            }

            lastWords.remove(0);
        }
    }

    /**
     * Set the default preferences at start.
     */
    private void setDefaulPreferences()
    {
        // Default value is set to line wrap the text
        cmiLineWrap.setSelected(true);

        // Default value is with word prediction
        cmiPrediction.setSelected(true);

        // Style default preferences
        setUserAgentStylesheet(STYLESHEET_CASPIAN);
    }

    /**
     * Prepares the application to close.
     */
    private void beforeClosing()
    {
        // Stop the thread responsible for the update of the most similar editorDocuments
        if (topicThread != null)
            topicThread.interrupt();

        dbPredictions.closeDBConnection();
        dbSuggestions.closeDBConnection();
    }
    
    /*--------------------------------------*/

    /**
     * Gets the stage to the controller and does other setups.
     *
     * @param stage the stage for this controller.
     */
    public void setStageAndSetupListeners(Stage stage)
    {
        this.stage = stage;

        // When the stage is closed:
        // - cancel the timer for the update topic editorDocuments
        stage.setOnCloseRequest((WindowEvent we) ->
                beforeClosing());
    }

    /**
     * @param s                the suggestion.
     * @param suggestionIndex  the index of the selected suggestions.
     * @param replacementIndex the index of the selected replacement.
     */
    public void applySuggestion(Sentence s, int suggestionIndex, int replacementIndex)
    {
        // Update the sentence in the list of sentences
        Suggestion sug = s.getSuggestions().get(suggestionIndex);
        String text = s.getSentence();

        String newWord = sug.getReplacements().get(replacementIndex).getReplacement();

        text = text.substring(0, sug.getFromPos()) + newWord + text.substring(sug.getToPos(), text.length());
        s.setSentence(text);
        s.setWasEdited(true);

        // Update the text in the editor itself
        TextArea selectedTextArea = editorDocuments.get(selectedIndex).getTextA();
        String completeText = selectedTextArea.getText();

        completeText = completeText.substring(0, sug.getFromPos() + s.getStartPos()) + newWord + completeText.substring(sug.getToPos() + s.getStartPos(), completeText.length());
        selectedTextArea.setText(completeText);

        // Remove all the suggestions that refer to the same sentence from the list and listview
        s.getSuggestions().clear();
        updateSuggestionsListView();

        updateAllSentences();

        printAllSentences();
    }

    public class MouseClickListCell2<T> extends ListCell<T>
    {
        @Override
        protected void updateItem(T item, boolean empty)
        {
            super.updateItem(item, empty);

            VBox vbox = new VBox();
            setGraphic(vbox);

            if (empty)
            {
                setText(null);
                setOnMouseClicked(null);
            }
            else
            {
                Sentence selectedSentence = (Sentence) getItem();

                int selectedSuggestion = (int) getListView().getUserData();

                setText(selectedSentence.getSuggestions().get(selectedSuggestion).getReplacements().get(getIndex()).getReplacement());

                setOnMouseClicked(event ->
                {
                    if (event.getClickCount() == 2)
                    {
                        applySuggestion(selectedSentence, selectedSuggestion, getIndex());
                    }
                });
            }
        }
    }

    /**
     * Get the index of the suggestion in the sentence, given the overall index of the suggestion.
     *
     * @param index the overall index of the suggestion.
     *
     * @return the index of the suggestion in the sentence.
     */
    private int getSentenceIndex(int index)
    {
        int i = 0;

        for (Sentence sentence : sentences)
            for (int j = 0; j < sentence.getSuggestions().size(); j++)
            {
                if (i == index)
                    return j;

                i++;
            }

        return -1;
    }

    /**
     * Based on:
     * eckig from http://stackoverflow.com/questions/28389389/listview-selecting-items-by-double-click
     * and
     * ItachiUchiha from http://stackoverflow.com/questions/22542015/how-to-add-a-mouse-doubleclick-event-listener-to-the-cells-of-a-listview-in-java
     * and
     * Andreas Pomarolli from https://examples.javacodegeeks.com/desktop-java/javafx/listview-javafx/javafx-listview-example/
     */
    public class MouseClickListCell<T> extends ListCell<T>
    {
        @Override
        protected void updateItem(T item, boolean empty)
        {
            super.updateItem(item, empty);

            VBox vbox = new VBox();
            setGraphic(vbox);

            if (empty)
            {
                setText(null);
                setOnMouseClicked(null);
            }
            else
            {
                Sentence s = (Sentence) item;

                int index = getSentenceIndex(getIndex());

                Suggestion sug = s.getSuggestions().get(index);

                int height = 20;

                final Label labelHeader = new Label("Replace \"" + s.getSentence().substring(sug.getFromPos(), sug.getToPos()) + "\" with:");
                labelHeader.setGraphicTextGap(10);
                labelHeader.setGraphic(createArrowPath(height, true));

                ListView<Sentence> lv = new ListView<>();
                lv.setUserData(index);

                setOnMouseClicked(event ->
                {
                    sug.setHidden(!sug.isHidden());

                    if (sug.isHidden())
                    {
                        labelHeader.setGraphic(createArrowPath(height, sug.isHidden()));
                        lv.getItems().clear();
                        vbox.getChildren().remove(1);
                    }
                    else
                    {
                        labelHeader.setGraphic(createArrowPath(height, sug.isHidden()));

                        for (SuggestedReplacement replacement : sug.getReplacements())
                            lv.getItems().add(s);

                        lv.setPrefHeight(lv.getItems().size() * (height + 5));

                        lv.setCellFactory(param -> new MouseClickListCell2<Sentence>());

                        vbox.getChildren().add(lv);

                        selectAffected(s, sug);
                    }
                });

                vbox.getChildren().add(labelHeader);
            }
        }

        private void selectAffected(Sentence s, Suggestion sug)
        {
            TextArea tArea = editorDocuments.get(selectedIndex).getTextA();

            tArea.positionCaret(s.getStartPos() + sug.getFromPos());
            tArea.selectPositionCaret(s.getStartPos() + sug.getToPos());
        }

        /**
         * Create an arrow for the listview item according to whether it is hidden or not.
         *
         * Based on:
         * Andreas Pomarolli from https://examples.javacodegeeks.com/desktop-java/javafx/listview-javafx/javafx-listview-example/
         *
         * @param height the height of the arrow.
         * @param hidden whether the item is hidden or not, changing the rotation of the arrow.
         *
         * @return the SVGPath that represents an arrow.
         */
        private SVGPath createArrowPath(int height, boolean hidden)
        {
            SVGPath svg = new SVGPath();
            int width = height / 4;

            if (hidden)
                svg.setContent("M0 " + (width * 2) + " L" + width + " " + width + " 0 0 Z");
            else
                svg.setContent("M0 0 L" + (width * 2) + " 0 L" + width + " " + width + " Z");

            return svg;
        }
    }
}